(function () {
  'use strict';

  angular
    .module('hzrequests.validators')
    .directive('validateUser', validateUser);

    validateUser.$inject = [
      '$q',
      'horizon.framework.util.http.service'
    ];

  function validateUser($q, serviceAPI) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: link
    };

    function link(scope, element, attr, ngModelCtrl) {
      ngModelCtrl.$asyncValidators.validateUser = function(modelValue, viewValue) {
        var value = modelValue || viewValue;
        return serviceAPI.post('/api/hzrequests/user/', {user: value}).then(
          function(response) {
            if (!response.data.status) {
              return $q.reject(); 
            }
            return true;
        })
      };
    }
  }
})();
