(function () {
  'use strict';

  angular
    .module('hzrequests.validators')
    .directive('validateGroup', validateGroup);

    validateGroup.$inject = [
      '$q',
      'horizon.framework.util.http.service'
    ];

  function validateGroup($q, serviceAPI) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: link
    };

    function link(scope, element, attr, ngModelCtrl) {
      ngModelCtrl.$asyncValidators.validateGroup = function(modelValue, viewValue) {
        var value = modelValue || viewValue;
        return serviceAPI.post('/api/hzrequests/groups/', {groups: value}).then(
          function(response) {
            if (!response.data.status) {
              return $q.reject(); 
            }
            return true;
        })
      };
    }
  }
})();
