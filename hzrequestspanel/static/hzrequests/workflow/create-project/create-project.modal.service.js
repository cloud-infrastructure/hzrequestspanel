(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .factory(
      'hzrequests.workflow.create-project.modal.service',
      CreateProjectModalService
    );

  CreateProjectModalService.$inject = [
    '$uibModal',
    '$window',
    'hzrequests.workflow.create-project.modal-spec'
  ];

  function CreateProjectModalService($uibModal, $window, modalSpec) {
    var service = {
      open: open
    };

    return service;

    function open(launchContext) {
      var localSpec = {
        resolve: {
          launchContext: function () {
            return launchContext;
          }
        }
      };

      angular.extend(localSpec, modalSpec);

      var createProjectModal = $uibModal.open(localSpec);
      var handleModalClose = function (redirectPropertyName) {
        return function () {
          if (launchContext && launchContext[redirectPropertyName]) {
            $window.location.href = launchContext[redirectPropertyName];
          }
        };
      };

      return createProjectModal.result.then(
        handleModalClose('successUrl'),
        handleModalClose('dismissUrl')
      );
    }
  }
})();