(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project', ['ngSanitize', 'ui.select'])
    .config(config)
    .constant('hzrequests.workflow.create-project.modal-spec', {
        backdrop: 'static',
        size: 'lg',
        controller: 'ModalContainerController',
        template: '<wizard class="wizard" ng-controller="CreateProjectWizardController"></wizard>'
      });

  config.$inject = [
    '$provide',
    '$windowProvider'
  ];

  /**
   * @name config
   * @param {Object} $provide
   * @param {Object} $windowProvider
   * @description Base path for the launch-instance code
   * @returns {undefined} No return value
   */
  function config($provide, $windowProvider) {
    var path = $windowProvider.$get().STATIC_URL + 'hzrequests/workflow/create-project/';
    $provide.constant('hzrequests.workflow.create-project.basePath', path);
  }
})();
