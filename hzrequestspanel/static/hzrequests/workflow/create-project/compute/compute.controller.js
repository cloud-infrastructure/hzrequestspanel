(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .controller('CreateProjectComputeController', CreateProjectComputeController);

  CreateProjectComputeController.$inject = [
    '$scope'
  ];

  function CreateProjectComputeController($scope) {
    var ctrl = this;
    var model = $scope.model;

    // Error text for invalid fields
    ctrl.projectComputeInstancesError = gettext('A valid instances number is required.');
    ctrl.projectComputeCoresError = gettext('A valid core number is required.');
    ctrl.projectComputeRAMError = gettext('A valid value of RAM is required.');

    ctrl.keepratio = true;

    ctrl.onChangeInstances = function() {
      if (ctrl.keepratio) {
        model.newProjectSpec.metadata.quota.compute.cores = model.newProjectSpec.metadata.quota.compute.instances;
        model.newProjectSpec.metadata.quota.compute.ram = model.newProjectSpec.metadata.quota.compute.instances * 2;
      }
    }

    ctrl.onChangeCores = function() {
      if (ctrl.keepratio) {
        model.newProjectSpec.metadata.quota.compute.instances = model.newProjectSpec.metadata.quota.compute.cores;
        model.newProjectSpec.metadata.quota.compute.ram = model.newProjectSpec.metadata.quota.compute.cores * 2;
      }
    }

    ctrl.onChangeRAM = function() {
      if (ctrl.keepratio) {
        model.newProjectSpec.metadata.quota.compute.instances = Math.floor(model.newProjectSpec.metadata.quota.compute.ram / 2);
        model.newProjectSpec.metadata.quota.compute.cores = Math.floor(model.newProjectSpec.metadata.quota.compute.ram / 2);
      }
    }
  }
})();
