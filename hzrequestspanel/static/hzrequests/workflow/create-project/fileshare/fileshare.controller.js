(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .controller('CreateProjectFileShareController', CreateProjectFileShareController);

  CreateProjectFileShareController.$inject = [
    '$scope',
    'horizon.app.core.openstack-service-api.settings'
  ];

  function CreateProjectFileShareController($scope, settingsService) {
    var ctrl = this;
    settingsService.getSetting('SHARE_TYPE_META').then(onShareTypeMeta);

    function compareType( a, b ) {
      if (a.name < b.name ) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    }

    function onShareTypeMeta(setting){
      ctrl.share_type_meta = [];
      for (var key in setting){
        var entry = {
          name: key,
          usage: setting[key].usage,
          type: setting[key].type,
          critical: setting[key].critical,
        };
        ctrl.share_type_meta.push(entry);
      }
      ctrl.share_type_meta.sort(compareType);
    }
  }
})();
