(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .controller('CreateProjectVolumeController', CreateProjectVolumeController);

  CreateProjectVolumeController.$inject = [
    '$scope',
    'horizon.app.core.openstack-service-api.settings'
  ];

  function CreateProjectVolumeController($scope, settingsService) {
    var ctrl = this;
    ctrl.projectVolumeBackupsError = gettext('A valid number of backups is required.');
    ctrl.projectVolumeBackupGigabytesError = gettext('A valid number of gigabytes is required.');
    settingsService.getSetting('VOLUME_TYPE_META').then(onVolumeTypeMeta);

    function compareType( a, b ) {
      if (a.name < b.name ) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    }

    function onVolumeTypeMeta(setting){
      ctrl.volume_type_meta = [];
      for (var key in setting){
        var entry = {
          name: key,
          usage: setting[key].usage,
          type: setting[key].type,
          min_iops: setting[key].min_iops,
          iops: setting[key].iops,
          throughput: setting[key].throughput
        };
        ctrl.volume_type_meta.push(entry);
      }
      ctrl.volume_type_meta.sort(compareType);
    }
  }
})();
