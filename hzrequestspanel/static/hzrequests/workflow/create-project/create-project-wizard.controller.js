(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .controller('CreateProjectWizardController', CreateProjectWizardController);

  CreateProjectWizardController.$inject = [
    '$scope',
    'createProjectModel',
    'hzrequests.workflow.create-project.workflow'
  ];

  function CreateProjectWizardController($scope, createProjectModel, createProjectWorkflow) {
    // Note: we set these attributes on the $scope so that the scope inheritance used all
    // through the launch instance wizard continues to work.
    $scope.workflow = createProjectWorkflow;        // eslint-disable-line angular/controller-as
    $scope.model = createProjectModel;              // eslint-disable-line angular/controller-as
    $scope.model.initialize(true);
    $scope.submit = $scope.model.createProject;  // eslint-disable-line angular/controller-as
  }
})();