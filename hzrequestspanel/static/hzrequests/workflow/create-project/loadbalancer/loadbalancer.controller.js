(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .controller('CreateProjectLoadbalancerController', CreateProjectLoadbalancerController);

  CreateProjectLoadbalancerController.$inject = [
    '$scope'
  ];

  function CreateProjectLoadbalancerController($scope) {
    var ctrl = this;
    var model = $scope.model;

    // Error text for invalid fields
    ctrl.projectLoadbalancerLoadBalancersError = gettext('A valid number of load balancers is required.');
    ctrl.projectLoadbalancerListenersError = gettext('A valid number of listeners is required.');
    ctrl.projectLoadbalancerPoolsError = gettext('A valid number of pools is required.');
    ctrl.projectLoadbalancerMembersError = gettext('A valid number of members is required.');
    ctrl.projectLoadbalancerHealthMonitorsError = gettext('A valid number of health monitors is required.');
    ctrl.projectLoadbalancerL7RulesError = gettext('A valid number of l7 rules is required.');
    ctrl.projectLoadbalancerL7PoliciesError = gettext('A valid number of l7 policies is required.');

    ctrl.keepratio = true;

    ctrl.onChangeLoadBalancers = function() {
      if (ctrl.keepratio) {
        model.newProjectSpec.metadata.quota.loadbalancer.listeners = model.newProjectSpec.metadata.quota.loadbalancer.loadbalancers * 5;
        model.newProjectSpec.metadata.quota.loadbalancer.pools = model.newProjectSpec.metadata.quota.loadbalancer.loadbalancers * 5;
        model.newProjectSpec.metadata.quota.loadbalancer.members = model.newProjectSpec.metadata.quota.loadbalancer.loadbalancers * 25;
        model.newProjectSpec.metadata.quota.loadbalancer.health_monitors = model.newProjectSpec.metadata.quota.loadbalancer.loadbalancers * 5;
        model.newProjectSpec.metadata.quota.loadbalancer.l7rules = model.newProjectSpec.metadata.quota.loadbalancer.loadbalancers * 100;
        model.newProjectSpec.metadata.quota.loadbalancer.l7policies = model.newProjectSpec.metadata.quota.loadbalancer.loadbalancers * 100;
      }
    }
  }
})();
