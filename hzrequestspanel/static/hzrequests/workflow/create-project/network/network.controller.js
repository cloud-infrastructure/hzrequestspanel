(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .controller('CreateProjectNetworkController', CreateProjectNetworkController);

  CreateProjectNetworkController.$inject = [
    '$scope'
  ];

  function CreateProjectNetworkController($scope) {
    var ctrl = this;
    // Error text for invalid fields
    ctrl.projectNetworkNetworksError = gettext('A valid number of Networks is required.');
    ctrl.projectNetworkSubnetsError = gettext('A valid number of Subnets is required.');
    ctrl.projectNetworkPortsError = gettext('A valid number of Ports is required.');
    ctrl.projectNetworkFloatingIpsError = gettext('A valid number of floating IPs is required.');
    ctrl.projectNetworkRoutersError = gettext('A valid number of Routers is required.');
    ctrl.projectNetworkSecurityGroupsError = gettext('A valid number of Security Groups is required.');
    ctrl.projectNetworkSecurityGroupRulesError = gettext('A valid number of Security Group Rules is required.');
  }
})();
