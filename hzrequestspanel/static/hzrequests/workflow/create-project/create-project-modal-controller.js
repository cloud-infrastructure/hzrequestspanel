(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .controller('CreateProjectModalController', CreateProjectModalController);

  CreateProjectModalController.$inject = [
    'hzrequests.workflow.create-project.modal.service'
  ];

  function CreateProjectModalController(modalService) {
    var ctrl = this;

    ctrl.openCreateProjectWizard = openCreateProjectWizard;

    function openCreateProjectWizard(launchContext) {
      modalService.open(launchContext);
    }
  }
})();
