(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .controller('CreateProjectDetailsController', CreateProjectDetailsController);

  CreateProjectDetailsController.$inject = [
    '$scope'
  ];

  function CreateProjectDetailsController($scope) {
    var ctrl = this;
    // Error text for invalid fields
    ctrl.projectNameError = gettext('A valid name is required for your project.');
    ctrl.projectDescriptionError = gettext('A valid description is required for your project.');
    ctrl.projectOwnerError = gettext('A valid primary account is required for your project.');
    ctrl.projectOwnerValidation = gettext('Checking username...');
    ctrl.projectEgroupError = gettext('A valid egroup is required for your project');
    ctrl.projectEgroupValidation = gettext('Checking groups...');
    ctrl.projectChargeGroupError = gettext('Please select one charge group');
    ctrl.projectCommentError = gettext('Please write a justification for your request.');
  }
})();
