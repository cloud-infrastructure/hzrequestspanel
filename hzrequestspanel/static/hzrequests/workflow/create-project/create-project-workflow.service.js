(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .factory('hzrequests.workflow.create-project.workflow', createProjectWorkflow);

  createProjectWorkflow.$inject = [
    'hzrequests.workflow.create-project.basePath',
    'horizon.app.core.workflow.factory'
  ];

  function createProjectWorkflow(basePath, dashboardWorkflow) {
    return dashboardWorkflow({
      title: gettext('Create a new project'),

      steps: [
        {
          id: 'details',
          title: gettext('Details'),
          templateUrl: basePath + 'details/details.html',
          helpUrl: basePath + 'details/details.help.html',
          formName: 'createProjectDetailsForm'
        },
        {
          id: 'compute',
          title: gettext('Compute'),
          templateUrl: basePath + 'compute/compute.html',
          helpUrl: basePath + 'compute/compute.help.html',
          formName: 'createProjectComputeForm'
        },
        {
          id: 'volume',
          title: gettext('Volumes'),
          templateUrl: basePath + 'volume/volume.html',
          helpUrl: basePath + 'volume/volume.help.html',
          formName: 'createProjectVolumeForm'
        },
        {
          id: 'fileshare',
          title: gettext('File Shares'),
          templateUrl: basePath + 'fileshare/fileshare.html',
          helpUrl: basePath + 'fileshare/fileshare.help.html',
          formName: 'createProjectFileShareForm'
        },
        {
          id: 'object',
          title: gettext('Object Storage'),
          templateUrl: basePath + 'object/object.html',
          helpUrl: basePath + 'object/object.help.html',
          formName: 'createProjectObjectForm'
        },
        {
          id: 'network',
          title: gettext('Network'),
          templateUrl: basePath + 'network/network.html',
          helpUrl: basePath + 'network/network.help.html',
          formName: 'createProjectNetworkForm'
        },
        {
          id: 'loadbalancer',
          title: gettext('Load Balancer'),
          templateUrl: basePath + 'loadbalancer/loadbalancer.html',
          helpUrl: basePath + 'loadbalancer/loadbalancer.help.html',
          formName: 'createProjectLoadBalancerForm',
        }
      ],

      btnText: {
        finish: gettext('Create New Project')
      },

      btnIcon: {
        finish: 'fa-plus'
      }
    });
  }
})();
