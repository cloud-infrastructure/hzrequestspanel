(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.create-project')
    .controller('CreateProjectObjectController', CreateProjectObjectController);

  CreateProjectObjectController.$inject = [
    '$scope'
  ];

  function CreateProjectObjectController($scope) {
    var ctrl = this;
    // Error text for invalid fields
    ctrl.projectObjectContainersError = gettext('A valid container number is required.');
    ctrl.projectObjectGigabytesError = gettext('A valid value of space is required.');
  }
})();
