(function () {
  'use strict';

  var push = Array.prototype.push;

  angular
    .module('hzrequests.workflow.create-project')
    .factory('createProjectModel', createProjectModel);

  createProjectModel.$inject = [
    '$q',
    'horizon.app.core.openstack-service-api.keystone',
    'horizon.framework.util.http.service',
    'horizon.framework.widgets.toast.service'
  ];

  function createProjectModel(
    $q,
    keystoneAPI,
    serviceAPI,
    toast,
  ) {

    var initPromise;

    var model = {
      newProjectSpec: {},
      chargegroups: [],
      volume_types: [],
      share_types: [],
      visible: {
        security_groups: false,
        security_group_rules: false,
        floatingips: false,
        networks: false,
        subnets: false,
        routers: false,
        buckets: false,
        gigabytes: false,
      },
      disabled: {
        security_groups: false,
        security_group_rules: false,
        floatingips: false,
        networks: false,
        subnets: false,
        routers: false,
        buckets: false,
        gigabytes: false,
      },
      loaded: {
        chargegroups: false,
        fileshare: false,
        volume: false
      },
      minimum: {
        compute: {
          instances: 0,
          cores: 0,
          ram: 0,
        },
        blockstorage: {},
        blockstorage_backup: {
          backups: 0,
          backup_gigabytes: 0,
        },
        fileshare: {},
        object: {
          buckets: 0,
          gigabytes: 0,
        },
        network: {
          networks: 0,
          subnets: 0,
          ports: 0,
          floatingips: 0,
          routers: 0,
          security_groups: 1,
          security_group_rules: 4,
        },
        loadbalancer: {
          loadbalancers: 0,
          listeners: 0,
          pools: 0,
          members: 0,
          health_monitors: 0,
          l7rules: 0,
          l7policies: 0,
        },
      },
      initialize: initialize,
      createProject: createProject
    }

    function initializeNewProjectSpec() {
      model.newProjectSpec = {
        ticket_type: 'new_project',
        username: null,
        comment: '',

        project_name: null,
        description: null,
        owner: null,
        egroup: null,
        services_region: null,

        chargegroup: null,

        metadata: {
          default: {
            egroup_responsible: true,
            egroup_mainuser: true,
          },
          quota: {
            compute: {
              instances: 10,
              cores: 10,
              ram: 20,
            },
            blockstorage: {},
            blockstorage_backup: {
              backups: 0,
              backup_gigabytes: 0,
            },
            fileshare: {},
            object: {
              buckets: 0,
              gigabytes: 0,
            },
            network: {
              networks: 0,
              subnets: 0,
              ports: 20,
              floatingips: 0,
              routers: 0,
              security_groups: 1,
              security_group_rules: 4,
            },
            loadbalancer: {
              loadbalancers: 0,
              listeners: 0,
              pools: 0,
              members: 0,
              health_monitors: 0,
              l7rules: 0,
              l7policies: 0,
            },
          }
        }
      };
    }

    function initializeLoadStatus() {
      angular.forEach(model.loaded, function(val, key) {
        model.loaded[key] = false;
      });
    }

    function initialize(deep) {
      var deferred, promise;

      // Each time opening create project wizard, we need to do this, or
      // we can call the whole methods `reset` instead of `initialize`.
      initializeNewProjectSpec();
      initializeLoadStatus();

      if (model.initializing) {
        promise = initPromise;

      } else if (model.initialized && !deep) {
        deferred = $q.defer();
        promise = deferred.promise;
        deferred.resolve();

      } else {
        toast.clearAll();
        model.initializing = true;

        var currentUserSession = keystoneAPI.getCurrentUserSession()

        promise = $q.all([
          currentUserSession.then(onGetCurrentUserSession).then(getRegionSettings).then(onRegionSettings),
          getChargeGroups().then(onChargeGroups).finally(onChargeGroupsComplete),
          getShareTypes().then(onShareTypeList).finally(onShareTypeListComplete),
          getVolumeTypes().then(onVolumeTypeList).finally(onVolumeTypeListComplete),
        ]);

        promise.then(onInitSuccess, onInitFail);
      }

      return promise;
    }

    function onInitSuccess() {
      model.initializing = false;
      model.initialized = true;
    }

    function onInitFail() {
      model.initializing = false;
      model.initialized = false;
    }

    function onGetCurrentUserSession(response) {
      model.newProjectSpec.username = response.data.username;
      model.newProjectSpec.owner = response.data.username;
      model.newProjectSpec.services_region = response.data.services_region;
    }

    function getRegionSettings(response) {
      return serviceAPI.get('/api/hzrequests/settings/'+model.newProjectSpec.services_region+'/');
    }

    function onRegionSettings(response){
      model.visible.security_groups = response.data.visible.security_groups;
      model.visible.security_group_rules = response.data.visible.security_group_rules;
      model.visible.floatingips = response.data.visible.floatingips;
      model.visible.networks = response.data.visible.networks;
      model.visible.subnets = response.data.visible.subnets;
      model.visible.routers = response.data.visible.routers;
      model.visible.buckets = response.data.visible.buckets;
      model.visible.object_gigabytes = response.data.visible.object_gigabytes;
      model.disabled.security_groups = response.data.disabled.security_groups;
      model.disabled.security_group_rules = response.data.disabled.security_group_rules;
      model.disabled.floatingips = response.data.disabled.floatingips;
      model.disabled.networks = response.data.disabled.networks;
      model.disabled.subnets = response.data.disabled.subnets;
      model.disabled.routers = response.data.disabled.routers;
      model.disabled.buckets = response.data.disabled.buckets;
      model.disabled.object_gigabytes = response.data.disabled.object_gigabytes;
    }

    function getChargeGroups() {
        return serviceAPI.get('/api/hzrequests/chargegroups');
    }

    function onChargeGroups(response) {
      model.chargegroups = [];
      push.apply(
        model.chargegroups,
        response.data.chargegroups.map(function (chargegroup) {
            return {
              uuid: chargegroup.uuid,
              name: chargegroup.name,
              category: chargegroup.category,
              org_unit: chargegroup.org_unit
            };
          })
        );
    }

    function onChargeGroupsComplete() {
      model.loaded.chargegroups = true;
    }

    function compareType( a, b ) {
      if (a.name < b.name ) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    }

    function getShareTypes(){
      return serviceAPI.get('/api/hzrequests/share_types');
    }

    function onShareTypeList(response){
      model.share_types = [];
      var len = response.data.share_types.length;
      for (var i=0; i < len; i++){
        model.share_types.push({
          'name': response.data.share_types[i].name
        });
        model.newProjectSpec.metadata.quota.fileshare[response.data.share_types[i].name] = {
          'shares': 0,
          'gigabytes': 0
        }
      }
      model.share_types.sort(compareType);
    }

    function onShareTypeListComplete() {
      model.loaded.fileshare = true;
    }

    function getVolumeTypes(){
      return serviceAPI.get('/api/hzrequests/volume_types');
    }

    function onVolumeTypeList(response){
      model.volume_types = [];
      var len = response.data.volume_types.length;
      for (var i=0; i < len; i++){
        model.volume_types.push({
          'name': response.data.volume_types[i].name
        });
        model.newProjectSpec.metadata.quota.blockstorage[response.data.volume_types[i].name] = {
          'volumes': 0,
          'gigabytes': 0
        }
      }
      model.volume_types.sort(compareType);
    }

    function onVolumeTypeListComplete() {
      model.loaded.volume = true;
    }

    function createProject() {
      var finalSpec = angular.copy(model.newProjectSpec);
      cleanNullProperties(finalSpec);
      finalizeRequest(finalSpec);
      // Send only the name of the chargegroup selected
      finalSpec.chargegroup = finalSpec.chargegroup.selected.name
      if (isRequestEmpty(finalSpec.metadata.quota)) {
        return Promise.reject('The request did not contain any resources requested. Please review it.');
      }
      return serviceAPI.post('/api/hzrequests/requests/', finalSpec).then(successMessage);
    }

    function finalizeRequest(finalSpec) {
      var region = finalSpec.services_region
      var quota = finalSpec.metadata.quota
      delete finalSpec.metadata['quota']
      finalSpec.metadata['quota'] = {}
      finalSpec.metadata['quota'][region] = quota
    }

    function isRequestEmpty(quota) {
      var zero = true
      for (let service in quota) {
        if (service == 'blockstorage' || service == 'fileshare') {
          for (let type in quota[service]) {
            for (let entry in quota[service][type]) {
              zero = zero && quota[service][type][entry] == 0
            }
          }
        } else {
          for (let entry in quota[service]) {
            zero = zero && quota[service][entry] == 0
          }
        }
      }
      return zero
    }

    function successMessage(response) {
        toast.add('success', gettext('Ticket created ' + response.data.ticket_number));
    }
  
    function cleanNullProperties(finalSpec) {
      // Initially clean fields that don't have any value.
      for (var key in finalSpec) {
        if (finalSpec.hasOwnProperty(key) && finalSpec[key] === null) {
          delete finalSpec[key];
        }
      }
    }

    return model;
  }
})();
