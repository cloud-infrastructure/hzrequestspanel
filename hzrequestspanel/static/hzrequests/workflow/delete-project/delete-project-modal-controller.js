(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.delete-project')
    .controller('DeleteProjectModalController', DeleteProjectModalController);

  DeleteProjectModalController.$inject = [
    'hzrequests.workflow.delete-project.modal.service'
  ];

  function DeleteProjectModalController(modalService) {
    var ctrl = this;

    ctrl.openDeleteProjectWizard = openDeleteProjectWizard;

    function openDeleteProjectWizard(launchContext) {
      modalService.open(launchContext);
    }
  }
})();
