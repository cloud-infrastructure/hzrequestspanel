(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.delete-project')
    .factory('hzrequests.workflow.delete-project.workflow', deleteProjectWorkflow);

  deleteProjectWorkflow.$inject = [
    'hzrequests.workflow.delete-project.basePath',
    'horizon.app.core.workflow.factory'
  ];

  function deleteProjectWorkflow(basePath, dashboardWorkflow) {
    return dashboardWorkflow({
      title: gettext('Delete this project'),

      steps: [
        {
          id: 'details',
          title: gettext('Details'),
          templateUrl: basePath + 'details/details.html',
          helpUrl: basePath + 'details/details.help.html',
          formName: 'deleteProjectDetailsForm'
        }
      ],

      btnText: {
        finish: gettext('Delete this Project')
      },

      btnIcon: {
        finish: 'fa-trash'
      }
    });
  }
})();
