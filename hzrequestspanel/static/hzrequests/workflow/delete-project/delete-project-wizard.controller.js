(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.delete-project')
    .controller('DeleteProjectWizardController', DeleteProjectWizardController);

  DeleteProjectWizardController.$inject = [
    '$scope',
    'deleteProjectModel',
    'hzrequests.workflow.delete-project.workflow'
  ];

  function DeleteProjectWizardController($scope, deleteProjectModel, deleteProjectWorkflow) {
    // Note: we set these attributes on the $scope so that the scope inheritance used all
    // through the launch instance wizard continues to work.
    $scope.workflow = deleteProjectWorkflow;        // eslint-disable-line angular/controller-as
    $scope.model = deleteProjectModel;              // eslint-disable-line angular/controller-as
    $scope.model.initialize(true);
    $scope.submit = $scope.model.deleteProject;  // eslint-disable-line angular/controller-as
  }
})();
