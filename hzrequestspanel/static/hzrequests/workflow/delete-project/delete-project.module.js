(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.delete-project', [])
    .config(config)
    .constant('hzrequests.workflow.delete-project.modal-spec', {
        backdrop: 'static',
        size: 'lg',
        controller: 'ModalContainerController',
        template: '<wizard class="wizard" ng-controller="DeleteProjectWizardController"></wizard>'
      });

  config.$inject = [
    '$provide',
    '$windowProvider'
  ];

  /**
   * @name config
   * @param {Object} $provide
   * @param {Object} $windowProvider
   * @description Base path for the launch-instance code
   * @returns {undefined} No return value
   */
  function config($provide, $windowProvider) {
    var path = $windowProvider.$get().STATIC_URL + 'hzrequests/workflow/delete-project/';
    $provide.constant('hzrequests.workflow.delete-project.basePath', path);
  }
})();
