(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.delete-project')
    .factory(
      'hzrequests.workflow.delete-project.modal.service',
      DeleteProjectModalService
    );

  DeleteProjectModalService.$inject = [
    '$uibModal',
    '$window',
    'hzrequests.workflow.delete-project.modal-spec'
  ];

  function DeleteProjectModalService($uibModal, $window, modalSpec) {
    var service = {
      open: open
    };

    return service;

    function open(launchContext) {
      var localSpec = {
        resolve: {
          launchContext: function () {
            return launchContext;
          }
        }
      };

      angular.extend(localSpec, modalSpec);

      var deleteProjectModal = $uibModal.open(localSpec);
      var handleModalClose = function (redirectPropertyName) {
        return function () {
          if (launchContext && launchContext[redirectPropertyName]) {
            $window.location.href = launchContext[redirectPropertyName];
          }
        };
      };

      return deleteProjectModal.result.then(
        handleModalClose('successUrl'),
        handleModalClose('dismissUrl')
      );
    }
  }
})();