(function () {
  'use strict';

  var push = Array.prototype.push;

  angular
    .module('hzrequests.workflow.delete-project')
    .factory('deleteProjectModel', deleteProjectModel);

  deleteProjectModel.$inject = [
    '$q',
    '$log',
    'horizon.app.core.openstack-service-api.keystone',
    'horizon.framework.util.http.service',
    'horizon.framework.widgets.toast.service'
  ];

  function deleteProjectModel(
    $q,
    $log,
    keystoneAPI,
    serviceAPI,
    toast,
  ) {

    var initPromise;

    var model = {
      deleteProjectSpec: {},
      project_name: '',
      regex: '',

      loaded: {
        // current_name on Details tab
        current_name: false
      },

      initialize: initialize,
      deleteProject: deleteProject
    }

    function initializeDeleteProjectSpec() {
      model.deleteProjectSpec = {
        ticket_type: 'delete_project',
        username: '',
        project_name: '',
        comment: '',
      };
    }

    function initializeLoadStatus() {
      angular.forEach(model.loaded, function(val, key) {
        model.loaded[key] = false;
      });
    }

    function initialize(deep) {
      var deferred, promise;

      // Each time opening launch instance wizard, we need to do this, or
      // we can call the whole methods `reset` instead of `initialize`.
      initializeDeleteProjectSpec();
      initializeLoadStatus();

      if (model.initializing) {
        promise = initPromise;

      } else if (model.initialized && !deep) {
        deferred = $q.defer();
        promise = deferred.promise;
        deferred.resolve();

      } else {
        toast.clearAll();
        model.initializing = true;

        promise = $q.all([
          keystoneAPI.getCurrentUserSession().then(onGetCurrentUserSession),
        ]);

        promise.then(onInitSuccess, onInitFail);
      }

      return promise;
    }

    function onInitSuccess() {
      model.initializing = false;
      model.initialized = true;
    }

    function onInitFail() {
      model.initializing = false;
      model.initialized = false;
    }

    function onGetCurrentUserSession(response) {
      model.project_name = response.data.project_name;
      model.regex = model.project_name.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
      model.deleteProjectSpec.username = response.data.username;
      model.loaded.current_name = true;
    }

    function deleteProject() {
      var finalSpec = angular.copy(model.deleteProjectSpec);
      cleanNullProperties(finalSpec);
      return serviceAPI.post('/api/hzrequests/requests/', finalSpec).then(successMessage);
    }

    function successMessage(response) {
        toast.add('success', gettext('Ticket created ' + response.data.ticket_number));
    }

    function cleanNullProperties(finalSpec) {
      // Initially clean fields that don't have any value.
      for (var key in finalSpec) {
        if (finalSpec.hasOwnProperty(key) && finalSpec[key] === null) {
          delete finalSpec[key];
        }
      }
    }

    return model;
  }
})();