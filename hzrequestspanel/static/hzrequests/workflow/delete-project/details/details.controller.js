(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.delete-project')
    .controller('DeleteProjectDetailsController', DeleteProjectDetailsController);

  DeleteProjectDetailsController.$inject = [
    '$scope'
  ];

  function DeleteProjectDetailsController($scope) {
    var ctrl = this;
    // Error text for invalid fields
    ctrl.projectNameError = gettext('The name needs to match with the project name in order to be deleted.');
  }
})();