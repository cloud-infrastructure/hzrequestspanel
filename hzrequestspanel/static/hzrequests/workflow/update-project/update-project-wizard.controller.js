(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .controller('UpdateProjectWizardController', UpdateProjectWizardController);

  UpdateProjectWizardController.$inject = [
    '$scope',
    'updateProjectModel',
    'hzrequests.workflow.update-project.workflow'
  ];

  function UpdateProjectWizardController($scope, updateProjectModel, updateProjectWorkflow) {
    // Note: we set these attributes on the $scope so that the scope inheritance used all
    // through the launch instance wizard continues to work.
    $scope.workflow = updateProjectWorkflow;        // eslint-disable-line angular/controller-as
    $scope.model = updateProjectModel;              // eslint-disable-line angular/controller-as
    $scope.model.initialize(true);
    $scope.submit = $scope.model.updateProject;  // eslint-disable-line angular/controller-as
  }
})();
