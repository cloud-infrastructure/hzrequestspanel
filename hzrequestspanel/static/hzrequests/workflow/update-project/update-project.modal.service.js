(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .factory(
      'hzrequests.workflow.update-project.modal.service',
      UpdateProjectModalService
    );

  UpdateProjectModalService.$inject = [
    '$uibModal',
    '$window',
    'hzrequests.workflow.update-project.modal-spec'
  ];

  function UpdateProjectModalService($uibModal, $window, modalSpec) {
    var service = {
      open: open
    };

    return service;

    function open(launchContext) {
      var localSpec = {
        resolve: {
          launchContext: function () {
            return launchContext;
          }
        }
      };

      angular.extend(localSpec, modalSpec);

      var updateProjectModal = $uibModal.open(localSpec);
      var handleModalClose = function (redirectPropertyName) {
        return function () {
          if (launchContext && launchContext[redirectPropertyName]) {
            $window.location.href = launchContext[redirectPropertyName];
          }
        };
      };

      return updateProjectModal.result.then(
        handleModalClose('successUrl'),
        handleModalClose('dismissUrl')
      );
    }
  }
})();