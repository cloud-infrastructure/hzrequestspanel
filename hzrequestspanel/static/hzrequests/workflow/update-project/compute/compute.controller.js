(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .controller('UpdateProjectComputeController', UpdateProjectComputeController);

  UpdateProjectComputeController.$inject = [
    '$scope'
  ];

  function UpdateProjectComputeController($scope) {
    var ctrl = this;
    var model = $scope.model;

    // Error text for invalid fields
    ctrl.projectComputeInstancesError = gettext('A valid instances number is required.');
    ctrl.projectComputeCoresError = gettext('A valid core number is required.');
    ctrl.projectComputeRAMError = gettext('A valid value of RAM is required.');

    ctrl.keepratio = true;

    ctrl.onChangeInstances = function() {
      if (ctrl.keepratio) {
        model.updateProjectSpec.metadata.quota.compute.cores = model.updateProjectSpec.metadata.quota.compute.instances;
        model.updateProjectSpec.metadata.quota.compute.ram = Math.floor(model.updateProjectSpec.metadata.quota.compute.instances * 2)
      }
      model.recalculate_ratios()
    }

    ctrl.onChangeCores = function() {
      if (ctrl.keepratio) {
        model.updateProjectSpec.metadata.quota.compute.instances = model.updateProjectSpec.metadata.quota.compute.cores;
        model.updateProjectSpec.metadata.quota.compute.ram = Math.floor(model.updateProjectSpec.metadata.quota.compute.cores * 2);
      }
      model.recalculate_ratios()
    }

    ctrl.onChangeRAM = function() {
      if (ctrl.keepratio) {
        model.updateProjectSpec.metadata.quota.compute.instances = Math.floor(model.updateProjectSpec.metadata.quota.compute.ram / 2);
        model.updateProjectSpec.metadata.quota.compute.cores = Math.floor(model.updateProjectSpec.metadata.quota.compute.ram / 2);
      }
      model.recalculate_ratios()
    }
  }
})();
