(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .controller('UpdateProjectDetailsController', UpdateProjectDetailsController);

  UpdateProjectDetailsController.$inject = [
    '$scope'
  ];

  function UpdateProjectDetailsController($scope) {
    var ctrl = this;
    // Error text for invalid fields
    ctrl.projectCommentError = gettext('Please write a justification for your request.');
  }
})();
