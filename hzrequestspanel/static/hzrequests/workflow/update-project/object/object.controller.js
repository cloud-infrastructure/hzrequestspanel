(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .controller('UpdateProjectObjectController', UpdateProjectObjectController);

  UpdateProjectObjectController.$inject = [
    '$scope'
  ];

  function UpdateProjectObjectController($scope) {
    var ctrl = this;
    // Error text for invalid fields
    ctrl.projectObjectContainersError = gettext('A valid container number is required.');
    ctrl.projectObjectGigabytesError = gettext('A valid value of space is required.');
  }
})();
