(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project', [])
    .config(config)
    .constant('hzrequests.workflow.update-project.modal-spec', {
        backdrop: 'static',
        size: 'lg',
        controller: 'ModalContainerController',
        template: '<wizard class="wizard" ng-controller="UpdateProjectWizardController"></wizard>'
      });
  

  config.$inject = [
    '$provide',
    '$windowProvider'
  ];

  /**
   * @name config
   * @param {Object} $provide
   * @param {Object} $windowProvider
   * @description Base path for the launch-instance code
   * @returns {undefined} No return value
   */
  function config($provide, $windowProvider) {
    var path = $windowProvider.$get().STATIC_URL + 'hzrequests/workflow/update-project/';
    $provide.constant('hzrequests.workflow.update-project.basePath', path);
  }
})();
