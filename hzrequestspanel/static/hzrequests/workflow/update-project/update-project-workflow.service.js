(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .factory('hzrequests.workflow.update-project.workflow', updateProjectWorkflow);

  updateProjectWorkflow.$inject = [
    'hzrequests.workflow.update-project.basePath',
    'horizon.app.core.workflow.factory'
  ];

  function updateProjectWorkflow(basePath, dashboardWorkflow) {
    return dashboardWorkflow({
      title: gettext('Update project'),

      steps: [
        {
          id: 'details',
          title: gettext('Details'),
          templateUrl: basePath + 'details/details.html',
          helpUrl: basePath + 'details/details.help.html',
          formName: 'updateProjectDetailsForm'
        },
        {
          id: 'compute',
          title: gettext('Compute'),
          templateUrl: basePath + 'compute/compute.html',
          helpUrl: basePath + 'compute/compute.help.html',
          formName: 'updateProjectComputeForm'
        },
        {
          id: 'volume',
          title: gettext('Volumes'),
          templateUrl: basePath + 'volume/volume.html',
          helpUrl: basePath + 'volume/volume.help.html',
          formName: 'updateProjectVolumeForm'
        },
        {
          id: 'fileshare',
          title: gettext('File Shares'),
          templateUrl: basePath + 'fileshare/fileshare.html',
          helpUrl: basePath + 'fileshare/fileshare.help.html',
          formName: 'updateProjectFileShareForm'
        },
        {
          id: 'object',
          title: gettext('Object Storage'),
          templateUrl: basePath + 'object/object.html',
          helpUrl: basePath + 'object/object.help.html',
          formName: 'updateProjectObjectForm'
        },
        {
          id: 'network',
          title: gettext('Network'),
          templateUrl: basePath + 'network/network.html',
          helpUrl: basePath + 'network/network.help.html',
          formName: 'updateProjectNetworkForm'
        },
        {
          id: 'loadbalancer',
          title: gettext('Load Balancer'),
          templateUrl: basePath + 'loadbalancer/loadbalancer.html',
          helpUrl: basePath + 'loadbalancer/loadbalancer.help.html',
          formName: 'updateProjectLoadBalancerForm',
        }
      ],

      btnText: {
        finish: gettext('Update project')
      },

      btnIcon: {
        finish: 'fa-edit'
      }
    });
  }
})();
