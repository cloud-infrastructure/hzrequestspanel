(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .controller('UpdateProjectLoadBalancerController', UpdateProjectLoadBalancerController);

  UpdateProjectLoadBalancerController.$inject = [
   '$scope'
  ];

  function UpdateProjectLoadBalancerController($scope) {
    var ctrl = this;
    var model = $scope.model;

    // Error text for invalid fields
    ctrl.projectLoadBalancerLoadBalancersError = gettext('A valid number of load balancers is required.');
    ctrl.projectLoadbalancerListenersError = gettext('A valid number of listeners is required.');
    ctrl.projectLoadbalancerPoolsError = gettext('A valid number of pools is required.');
    ctrl.projectLoadbalancerMembersError = gettext('A valid number of members is required.');
    ctrl.projectLoadbalancerHealthMonitorsError = gettext('A valid number of health monitors is required.');
    ctrl.projectLoadbalancerL7RulesError = gettext('A valid number of l7 rules is required.');
    ctrl.projectLoadbalancerL7PoliciesError = gettext('A valid number of l7 policies is required.');

    ctrl.keepratio = false;

    ctrl.onChangeLoadBalancers = function() {
      if (ctrl.keepratio) {
        model.updateProjectSpec.metadata.quota.loadbalancer.listeners = model.updateProjectSpec.metadata.quota.loadbalancer.loadbalancers * 5;
        model.updateProjectSpec.metadata.quota.loadbalancer.pools = model.updateProjectSpec.metadata.quota.loadbalancer.loadbalancers * 5;
        model.updateProjectSpec.metadata.quota.loadbalancer.members = model.updateProjectSpec.metadata.quota.loadbalancer.loadbalancers * 25;
        model.updateProjectSpec.metadata.quota.loadbalancer.health_monitors = model.updateProjectSpec.metadata.quota.loadbalancer.loadbalancers * 5;
        model.updateProjectSpec.metadata.quota.loadbalancer.l7rules = model.updateProjectSpec.metadata.quota.loadbalancer.loadbalancers * 100;
        model.updateProjectSpec.metadata.quota.loadbalancer.l7policies = model.updateProjectSpec.metadata.quota.loadbalancer.loadbalancers * 100;
      }
      model.recalculate_ratios()
    }
  }
})();
