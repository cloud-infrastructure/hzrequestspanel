(function () {
  'use strict';

  var push = Array.prototype.push;

  angular
    .module('hzrequests.workflow.update-project')
    .factory('updateProjectModel', updateProjectModel);

  updateProjectModel.$inject = [
    '$q',
    'horizon.app.core.openstack-service-api.keystone',
    'horizon.framework.util.http.service',
    'horizon.framework.widgets.toast.service'
  ];

  function updateProjectModel(
    $q,
    keystoneAPI,
    serviceAPI,
    toast,
  ) {

    var initPromise;

    var model = {
      updateProjectSpec: {},

      chargegroup: null,
      volume_types: [],
      share_types: [],

      ratio: {
        compute: {
          instances: null,
          cores: null,
          ram: null,
        },
        blockstorage: {},
        blockstorage_backup: {
          backups: null,
          backup_gigabytes: null,
        },
        fileshare: {},
        object: {
          buckets: null,
          gigabytes: null,
        },
        network: {
          networks: null,
          subnets: null,
          ports: null,
          floatingips: null,
          routers: null,
          security_groups: null,
          security_group_rules: null,
        },
        loadbalancer: {
          loadbalancers: null,
          listeners: null,
          pools: null,
          members: null,
          health_monitors: null,
          l7rules: null,
          l7policies: null,
        },
      },
      minimum: {
        compute: {
          instances: 0,
          cores: 0,
          ram: 0,
        },
        blockstorage: {},
        blockstorage_backup: {
          backups: 0,
          backup_gigabytes: 0,
        },
        fileshare: {},
        object: {
          buckets: 0,
          gigabytes: 0,
        },
        network: {
          networks: 0,
          subnets: 0,
          ports: 0,
          floatingips: 0,
          routers: 0,
          security_groups: 0,
          security_group_rules: 0,
        },
        loadbalancer: {
          loadbalancers: 0,
          listeners: 0,
          pools: 0,
          members: 0,
          health_monitors: 0,
          l7rules: 0,
          l7policies: 0,
        },
      },
      visible: {
        security_groups: false,
        security_group_rules: false,
        floatingips: false,
        networks: false,
        subnets: false,
        routers: false,
        buckets: false,
        gigabytes: false,
      },
      disabled: {
        security_groups: false,
        security_group_rules: false,
        floatingips: false,
        networks: false,
        subnets: false,
        routers: false,
        buckets: false,
        gigabytes: false,
      },
      loaded: {
        chargegroups: false,
        compute: false,
        volume: false,
        fileshare: false,
        object: false,
        network: false,
        loadbalancer: false,
      },

      initialize: initialize,
      updateProject: updateProject,
      recalculate_ratios: recalculate_ratios,
    }

    function initializeUpdateProjectSpec() {
      model.updateProjectSpec = {
        ticket_type: 'quota_change',
        username: null,
        project_name: null,
        services_region: null,
        comment: '',
        metadata: {
          quota: {
            compute: {
              instances: null,
              cores: null,
              ram: null,
            },
            blockstorage: {},
            blockstorage_backup: {
              backups: null,
              backup_gigabytes: null,
            },
            fileshare: {},
            object: {
              buckets: null,
              gigabytes: null,
            },
            network: {
              networks: null,
              subnets: null,
              ports: null,
              floatingips: null,
              routers: null,
              security_groups: null,
              security_group_rules: null,
            },
            loadbalancer: {
              loadbalancers: null,
              listeners: null,
              pools: null,
              members: null,
              health_monitors: null,
              l7rules: null,
              l7policies: null,
            },
          },
          current: {
            compute: {
              instances: null,
              cores: null,
              ram: null,
            },
            blockstorage: {},
            blockstorage_backup: {
              backups: null,
              gigabytes: null,
            },
            fileshare: {},
            object: {
              buckets: null,
              gigabytes: null,
            },
            network: {
              networks: null,
              subnets: null,
              ports: null,
              floatingips: null,
              routers: null,
              security_groups: null,
              security_group_rules: null,
            },
            loadbalancer: {
              loadbalancers: null,
              listeners: null,
              pools: null,
              members: null,
              health_monitors: null,
              l7rules: null,
              l7policies: null,
            },
          }
        }
      };
    }

    function initializeLoadStatus() {
      angular.forEach(model.loaded, function(val, key) {
        model.loaded[key] = false;
      });
    }

    function initialize(deep) {
      var deferred, promise;

      // Each time opening create project wizard, we need to do this, or
      // we can call the whole methods `reset` instead of `initialize`.
      initializeUpdateProjectSpec();
      initializeLoadStatus();

      if (model.initializing) {
        promise = initPromise;

      } else if (model.initialized && !deep) {
        deferred = $q.defer();
        promise = deferred.promise;
        deferred.resolve();

      } else {
        toast.clearAll();
        model.initializing = true;

        var currentUserSession = keystoneAPI.getCurrentUserSession()

        promise = $q.all([
          currentUserSession.then(onGetCurrentUserSession).then(getRegionSettings).then(onRegionSettings),
          currentUserSession.then(getProjectInfo).then(onProjectInfo).then(getChargeGroups).then(onChargeGroups, onChargeGroupsError),
          getProjectQuota().then(onProjectQuota).finally(onProjectQuotaComplete)
        ]);

        promise.then(onInitSuccess, onInitFail);
      }

      return promise;
    }

    function onInitSuccess() {
      model.initializing = false;
      model.initialized = true;
    }

    function onInitFail() {
      model.initializing = false;
      model.initialized = false;
    }

    function onGetCurrentUserSession(response) {
      model.updateProjectSpec.username = response.data.username;
      model.updateProjectSpec.services_region = response.data.services_region;
    }

    function getRegionSettings(response) {
      return serviceAPI.get('/api/hzrequests/settings/'+model.updateProjectSpec.services_region+'/');
    }

    function onRegionSettings(response){
      model.visible.security_groups = response.data.visible.security_groups;
      model.visible.security_group_rules = response.data.visible.security_group_rules;
      model.visible.floatingips = response.data.visible.floatingips;
      model.visible.networks = response.data.visible.networks;
      model.visible.subnets = response.data.visible.subnets;
      model.visible.routers = response.data.visible.routers;
      model.visible.buckets = response.data.visible.buckets;
      model.visible.object_gigabytes = response.data.visible.object_gigabytes;
      model.disabled.security_groups = response.data.disabled.security_groups;
      model.disabled.security_group_rules = response.data.disabled.security_group_rules;
      model.disabled.floatingips = response.data.disabled.floatingips;
      model.disabled.networks = response.data.disabled.networks;
      model.disabled.subnets = response.data.disabled.subnets;
      model.disabled.routers = response.data.disabled.routers;
      model.disabled.buckets = response.data.disabled.buckets;
      model.disabled.object_gigabytes = response.data.disabled.object_gigabytes;
    }

    function getProjectInfo(response) {
      return keystoneAPI.getProject(response.data.project_id);
    }

    function onProjectInfo(response) {
      model.updateProjectSpec.project_name = response.data.name;
      model.chargegroup_id = response.data['chargegroup'];
    }

    function getChargeGroups() {
      return serviceAPI.get('/api/hzrequests/chargegroups/'+model.chargegroup_id+'/');
    }

    function onChargeGroups(response) {
      model.chargegroups = {};
      response.data.chargegroups.map(function (chargegroup) {
        model.chargegroups[chargegroup.uuid] = chargegroup;
      });
      model.chargegroup = model.chargegroups[model.chargegroup_id].name;
      model.loaded.chargegroups = true;
    }

    function onChargeGroupsError(response) {
      model.chargegroups = {};
      model.chargegroup = "(undefined)";
      model.loaded.chargegroups = true;
    }

    function getProjectQuota() {
      return serviceAPI.get('/api/hzrequests/project_quotas');
    }

    function onProjectQuota(response) {
      var region = model.updateProjectSpec.services_region
      model.updateProjectSpec.metadata.quota = response.data.quota[region];
      model.updateProjectSpec.metadata.current = getCurrentDict(response.data.quota[region]);
      model.minimum = response.data.current[region];
      initializeModelRatioAndTypes(response.data.quota[region]);
    }

    function onProjectQuotaComplete() {
      model.loaded.compute = true;
      model.loaded.volume = true;
      model.loaded.fileshare = true;
      model.loaded.object = true;
      model.loaded.network = true;
      model.loaded.loadbalancer = true;
    }

    function getCurrentDict(quota) {
      var current = {};
      for (let service in quota) {
        current[service] = {};
        if (service == 'blockstorage' || service == 'fileshare') {
          for (let type in quota[service]) {
            current[service][type] = {}
            for (let entry in quota[service][type]) {
              current[service][type][entry] = quota[service][type][entry]
            }
          }
        } else {
          for (let entry in quota[service]) {
            current[service][entry] = quota[service][entry]
          }
        }
      }
      return current;
    }

    function initializeModelRatioAndTypes(quota) {
      model.volume_types = [];
      model.share_types = [];
      for (let service in quota) {
        if (service == 'blockstorage' || service == 'fileshare') {
          for (let type in quota[service]) {
            model.ratio[service][type] = {};
            for (let entry in quota[service][type]) {
              model.ratio[service][type][entry] = '';
            }
            if (service == 'blockstorage') {
              model.volume_types.push({'name': type});
            } else if (service == 'fileshare') {
              model.share_types.push({'name': type});
            }
          }
        } else {
          for (let entry in quota[service]) {
            model.ratio[service][entry] = '';
          }
        }
      }
      model.volume_types.sort(compareType);
      model.share_types.sort(compareType);
    }

    function compareType( a, b ) {
      if (a.name < b.name ) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    }

    function updateProject() {
      var finalSpec = angular.copy(model.updateProjectSpec);
      cleanNullProperties(finalSpec);
      finalizeRequest(finalSpec);
      if (isRequestEmpty(finalSpec.metadata.quota,finalSpec.metadata.current)) {
        return Promise.reject('The request did not have any changes to be made. Please review it.');
      }
      return serviceAPI.post('/api/hzrequests/requests/', finalSpec).then(successMessage);
    }

    function isRequestEmpty(quota, initial) {
      var zero = true
      for (let service in quota) {
        if (service == 'blockstorage' || service == 'fileshare') {
          for (let type in quota[service]) {
            for (let entry in quota[service][type]) {
              zero = zero && quota[service][type][entry] == initial[service][type][entry]
            }
          }
        } else {
          for (let entry in quota[service]) {
            zero = zero && quota[service][entry] == initial[service][entry]
          }
        }
      }
      return zero
    }

    function successMessage(response) {
        toast.add('success', gettext('Ticket created ' + response.data.ticket_number));
    }

    function cleanNullProperties(finalSpec) {
      // Initially clean fields that don't have any value.
      for (var key in finalSpec) {
        if (finalSpec.hasOwnProperty(key) && finalSpec[key] === null) {
          delete finalSpec[key];
        }
      }
    }

    function finalizeRequest(finalSpec) {
      var region = finalSpec.services_region
      var quota = finalSpec.metadata.quota
      var current = finalSpec.metadata.current
      delete finalSpec.metadata['quota']
      delete finalSpec.metadata['current']
      finalSpec.metadata['quota'] = {}
      finalSpec.metadata['quota'][region] = quota
      finalSpec.metadata['current'] = {}
      finalSpec.metadata['current'][region] = current
    }

    function recalculate_ratios() {
      var quota = model.updateProjectSpec.metadata.quota
      var current = model.updateProjectSpec.metadata.current

      for (let service in quota) {
        if (service == 'blockstorage' || service == 'fileshare') {
          for (let type in quota[service]) {
            for (let entry in quota[service][type]) {
              model.ratio[service][type][entry] = calculate_ratio(
                quota[service][type][entry],
                current[service][type][entry]
              )
            }
          }
        } else {
          for (let entry in quota[service]) {
            model.ratio[service][entry] = calculate_ratio(
              quota[service][entry],
              current[service][entry]
            )
          }
        }
      }
      // If the user is requesting some instances there should be some associated resources as well
      if (quota.compute.instances > current.compute.instances) {
        if (current.network.ports < quota.compute.instances) {
          model.minimum.network.ports = quota.compute.instances;
        }
        if (current.network.security_groups == 0) {
          model.minimum.network.security_groups = 1;
        }
        if (current.network.security_group_rules == 0) {
          model.minimum.network.security_group_rules = 4;
        }
      } else if (quota.compute.instances <= current.compute.instances) {
        model.minimum.network.ports = current.network.ports;
        model.minimum.network.security_groups = current.network.security_groups;
        model.minimum.network.security_group_rules = current.network.security_group_rules;
      }
    }

    function calculate_ratio(desired, current){
      var percentage = parseInt(((100 * desired) / current) - 100);
      var number = (desired - current);
      if (isNaN(number)) {
        number = 0;
      }
      if (isNaN(percentage)){ 
        percentage = 0;
      }
      var sign = '';
      if (number > 0) {
        sign = '+';
      }

      // Only display info if it is != 0
      if (number == 0 && percentage == 0) {
          return '';
      } else {
          return sign + number + ' (' + sign + '' + percentage + '%)';
      }
  }

    return model;
  }
})();
