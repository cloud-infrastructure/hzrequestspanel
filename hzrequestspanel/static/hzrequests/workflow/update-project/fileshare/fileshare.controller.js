(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .controller('UpdateProjectFileShareController', UpdateProjectFileShareController);

  UpdateProjectFileShareController.$inject = [
    '$scope',
    'horizon.app.core.openstack-service-api.settings'
  ];

  function UpdateProjectFileShareController($scope, settingsService) {
    var ctrl = this;
    settingsService.getSetting('SHARE_TYPE_META').then(onShareTypeMeta);

    function compareType( a, b ) {
      if (a.name < b.name ) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    }

    function onShareTypeMeta(setting){
      ctrl.share_type_meta = [];
      for (var key in setting){
        var entry = {
          name: key,
          usage: setting[key].usage,
          type: setting[key].type,
          critical: setting[key].critical,
        };
        ctrl.share_type_meta.push(entry);
      }
      ctrl.share_type_meta.sort(compareType);
    }
  }
})();
