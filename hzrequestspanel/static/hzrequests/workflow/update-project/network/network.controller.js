(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .controller('UpdateProjectNetworkController', UpdateProjectNetworkController);

  UpdateProjectNetworkController.$inject = [
   '$scope'
  ];

  function UpdateProjectNetworkController($scope) {
    var ctrl = this;

    // Error text for invalid fields
    ctrl.projectNetworkLoadBalancersError = gettext('A valid number of load balancers is required.');
    ctrl.projectNetworkFloatingIpsError = gettext('A valid number of floating IPs is required.');
  }
})();
