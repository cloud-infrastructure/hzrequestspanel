(function () {
  'use strict';

  angular
    .module('hzrequests.workflow.update-project')
    .controller('UpdateProjectModalController', UpdateProjectModalController);

  UpdateProjectModalController.$inject = [
    'hzrequests.workflow.update-project.modal.service'
  ];

  function UpdateProjectModalController(modalService) {
    var ctrl = this;

    ctrl.openUpdateProjectWizard = openUpdateProjectWizard;

    function openUpdateProjectWizard(launchContext) {
      modalService.open(launchContext);
    }
  }
})();
