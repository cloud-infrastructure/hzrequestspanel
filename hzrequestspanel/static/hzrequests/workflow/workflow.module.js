(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name hzrequests.workflow
   * @description
   * module to host workflow modules.
   */
  angular
    .module('hzrequests.workflow', [
      'hzrequests.workflow.create-project',
      'hzrequests.workflow.delete-project',
      'hzrequests.workflow.update-project'
   ]);
})();
