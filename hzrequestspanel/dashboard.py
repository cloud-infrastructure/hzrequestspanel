from django.utils.translation import gettext_lazy as _

import horizon


class HZRequests(horizon.Dashboard):
    name = _("CERN Cloud Service")
    slug = "hzrequests"
    panels = ('overview', 'support')
    default_panel = 'overview'


horizon.register(HZRequests)
