from django.utils.translation import gettext_lazy as _

import horizon

from hzrequestspanel import dashboard


class OverviewPanel(horizon.Panel):
    name = _("Overview")
    slug = "overview"


dashboard.HZRequests.register(OverviewPanel)
