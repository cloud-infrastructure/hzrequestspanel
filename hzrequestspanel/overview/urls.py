from django.urls import re_path
from hzrequestspanel.api import hzrequests_rest_api
from hzrequestspanel.overview import views


urlpatterns = [
    re_path(r'^$', views.IndexView.as_view(), name='index'),
    re_path(hzrequests_rest_api.HZRequest.url_regex,
            hzrequests_rest_api.HZRequest.as_view(),
            name='createrequest'),
]
