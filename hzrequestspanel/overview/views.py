import collections

from ccitools.utils.cloud import CloudRegionClient
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from keystoneauth1.identity import v3
from keystoneauth1 import session
from openstack_dashboard import usage
from typing import Iterable
from typing import List
from typing import TypeVar

SERVICES_ON_DEMAND = {
    'object': 's3'
}

VOLUMES_PRIORITY = [
    'standard', 'io1', 'io2', 'io3', 'cp1', 'cpio1', 'cpio2', 'cpio3'
]


T = TypeVar("T")


def sort_volume_by_priority(values: Iterable[T]) -> List[T]:
    priority_dict = {k: i for i, k in enumerate(VOLUMES_PRIORITY)}

    def priority_getter(value):
        return priority_dict.get(value, len(values))
    return sorted(values, key=priority_getter)


ChartDef = collections.namedtuple(
    'ChartDef',
    ('element', 'title', 'service', 'sort', 'field'))

ROW_DEFS = [
    {
        'charts': [
            ChartDef(
                "Compute",
                _("Compute"),
                'compute',
                False,
                None),
        ]
    },
    {
        'charts': [
            ChartDef(
                "Volume",
                _("Number of Volumes"),
                'blockstorage',
                sort_volume_by_priority,
                'volumes'),
            ChartDef(
                "VolumeGigabytes",
                _("Volume size [GB]"),
                'blockstorage',
                sort_volume_by_priority,
                'gigabytes'),
            ChartDef(
                "VolumeBackups",
                _("Volume Backups"),
                'blockstorage_backup',
                False,
                None),
        ]
    },
    {
        'charts': [
            ChartDef(
                "FileShare",
                _("Number of File Shares"),
                'fileshare',
                sorted,
                'shares'),
            ChartDef(
                "FileShareGigabytes",
                _("File Share size [GB]"),
                'fileshare',
                sorted,
                'gigabytes'),
        ]
    },
    {
        'charts': [
            ChartDef(
                "ObjectStorage",
                _("Object Storage"),
                'object',
                False,
                None),
        ]
    },
    {
        'charts': [
            ChartDef(
                "Network",
                _("Network"),
                'network',
                False,
                None),
            ChartDef(
                "LoadBalancer",
                _("Load Balancer"),
                'loadbalancer',
                False,
                None),
        ]
    },
]


class IndexView(usage.UsageView):
    table_class = usage.ProjectUsageTable
    usage_class = usage.ProjectUsage
    template_name = 'hzrequestspanel/overview/index.html'
    page_title = _("Overview")

    def _get_charts_data(self):
        chart_sections = []
        for section in ROW_DEFS:
            chart_sections.append({
                'charts': self._process_chart_rows(section['charts'])
            })
        return chart_sections

    def _process_chart_rows(self, chart_defs):
        region = self.request.session['services_region']
        charts = []
        for t in chart_defs:
            if t.service in self.filters:
                continue

            try:
                quota = self.quota['quota'][region][t.service]
                current = self.quota['current'][region][t.service]
            except KeyError:
                continue

            if not quota and not current:
                continue

            dataset = []
            for k in t.sort(sorted(quota.keys())) if t.sort else quota.keys():
                try:
                    used = round(current[k][
                        t.field] if t.field else current[k])
                    limit = round(quota[k][t.field] if t.field else quota[k])

                    if limit == -1:
                        limit = None
                        free = None
                    else:
                        free = limit - used

                    dataset.append({
                        'name': k,
                        'used': used if used else None,
                        'limit': limit if limit else None,
                        'free': free if free else None,
                    })
                except KeyError:
                    continue

            if dataset:
                charts.append({
                    'element': t.element,
                    'title': t.title,
                    'dataset': dataset,
                    'min_height': '{0}px'.format(
                        (int(len(dataset) / 6) + 1) * 200)
                })
        return charts

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['charts'] = self._get_charts_data()
        context['isShared'] = not self.request.user.project_name.startswith(
            u'Personal')
        return context

    def get_data(self):
        data = super(IndexView, self).get_data()
        client = CloudRegionClient(cloud='cern')
        region = self.request.session['services_region']
        self.quota = client.get_project_quota(
            project_id=self.request.user.project_id,
            filter=region,
            session=session.Session(
                auth=v3.Token(
                    auth_url=settings.OPENSTACK_KEYSTONE_URL,
                    token=self.request.user.token.id,
                    project_id=self.request.user.tenant_id
                )
            )
        )

        self.filters = []
        for svc, ep in SERVICES_ON_DEMAND.items():
            if not client.check_endpoint_group(
                    name=ep, project_id=self.request.user.project_id):
                self.filters.append(svc)

        return data
