from ccitools import common
from ccitools.utils.cloud import CloudRegionClient
import configparser
from django.conf import settings
from django.core.cache import cache
from django import http
from django.views import generic

from hzrequestspanel.api.projects import project_creator
from hzrequestspanel.api.projects import project_killer
from hzrequestspanel.api.projects import quota_changer
from keystoneauth1.identity import v3
from keystoneauth1 import session

from openstack_dashboard.api import keystone
from openstack_dashboard.api.rest import urls
from openstack_dashboard.api.rest import utils as rest_utils


@urls.register
class HZRequest(generic.View):
    """API for requests."""

    url_regex = r'hzrequests/requests/$'

    @rest_utils.ajax(data_required=True)
    def post(self, request):
        if request.DATA['ticket_type'] == 'new_project':
            ticket = project_creator.NewProjectCreator(request.DATA)
        elif request.DATA['ticket_type'] == 'quota_change':
            ticket = quota_changer.QuotaChanger(request.DATA)
        elif request.DATA['ticket_type'] == 'delete_project':
            ticket = project_killer.ProjectKiller(request.DATA)

        return {"ticket_number": ticket.create_ticket()}


@urls.register
class ChargeGroups(generic.View):
    """API for fetching chargegroups."""

    url_regex = r'hzrequests/chargegroups/$'

    @rest_utils.ajax()
    def get(self, request):
        chargegroups = cache.get('chargegroups')
        if not chargegroups:
            try:
                chargegroups = common.get_chargegroups()
            except Exception:
                chargegroups = []
        if chargegroups:
            cache.set(
                'chargegroups',
                chargegroups,
                settings.CHARGEGROUPS_CACHE_TIMEOUT)
        return http.JsonResponse(
            data={'chargegroups': chargegroups})


@urls.register
class ChargeGroup(generic.View):
    """API for fetching chargegroup by uuid."""

    url_regex = r'hzrequests/chargegroups/(?P<uuid>[^/]+)/$'

    @rest_utils.ajax()
    def get(self, request, uuid):
        chargegroups = common.get_chargegroups(uuid=uuid, inactive=True)
        if chargegroups:
            return http.JsonResponse(
                data={'chargegroups': chargegroups})
        else:
            return http.HttpResponseNotFound(
                content='Chargegroup {0} Not Found'.format(uuid))


@urls.register
class UserValidationView(generic.View):
    """API for users."""

    url_regex = r'hzrequests/user/$'

    @rest_utils.ajax(data_required=True)
    def post(self, request):
        try:
            keystone.user_get(request, request.DATA['user'], admin=False)
        except Exception:
            return http.HttpResponseNotFound(
                content='User {0} Not Found'.format(request.DATA['user']))
        return http.JsonResponse(data={'status': True})


@urls.register
class GroupValidationView(generic.View):
    """API for groups."""

    url_regex = r'hzrequests/groups/$'
    config_file = '/etc/openstack-dashboard/hzrequestspanel.conf'

    @rest_utils.ajax(data_required=True)
    def post(self, request):
        try:
            config = configparser.ConfigParser()
            with open(self.config_file, 'r', encoding='utf-8') as conf_fp:
                config.read_file(conf_fp)
                group_blacklist = [
                    group.lower() for group in config.get(
                        "servicenow", "group_blacklist").split(',')]
        except Exception:
            group_blacklist = []

        for group in request.DATA['groups'].split(','):
            try:
                keystone.group_get(request, group, admin=False)
            except Exception:
                return http.HttpResponseNotFound(
                    content='Group {0} Not Found'.format(group))
            if group in group_blacklist:
                return http.JsonResponse(data={'status': False})
        return http.JsonResponse(data={'status': True})


@urls.register
class ProjectQuotas(generic.View):
    """API for project quotas."""

    url_regex = r'hzrequests/project_quotas/$'

    def get(self, request):
        client = CloudRegionClient(cloud='cern')
        region = request.session['services_region']
        project_quota = client.get_project_quota(
            project_id=request.user.project_id,
            filter=region,
            session=session.Session(
                auth=v3.Token(
                    auth_url=settings.OPENSTACK_KEYSTONE_URL,
                    token=request.user.token.id,
                    project_id=request.user.tenant_id
                )
            )
        )
        return http.JsonResponse(data=project_quota)


@urls.register
class VolumeTypes(generic.View):
    """API for Cinder volume types."""

    url_regex = r'hzrequests/volume_types/$'

    def get(self, request):
        client = CloudRegionClient(cloud='cern')
        region = request.session['services_region']
        volume_types = client.get_volume_types(
            region=region,
            session=session.Session(
                auth=v3.Token(
                    auth_url=settings.OPENSTACK_KEYSTONE_URL,
                    token=request.user.token.id,
                    project_id=request.user.tenant_id
                )
            ),
            show_all=False)

        return http.JsonResponse(
            data={
                'volume_types': [{'name': vt.name} for vt in volume_types]
            }
        )


@urls.register
class ShareTypes(generic.View):
    """API for manila share types."""

    url_regex = r'hzrequests/share_types/$'

    def get(self, request):
        client = CloudRegionClient(cloud='cern')
        region = request.session['services_region']
        share_types = client.get_share_types(
            region=region,
            session=session.Session(
                auth=v3.Token(
                    auth_url=settings.OPENSTACK_KEYSTONE_URL,
                    token=request.user.token.id,
                    project_id=request.user.tenant_id
                )
            ),
            show_all=False)

        return http.JsonResponse(
            data={
                'share_types': [{'name': st.name} for st in share_types]
            }
        )


@urls.register
class Settings(generic.View):
    """API for retrieving region settings."""

    url_regex = r'hzrequests/settings/(?P<region>[^/]+)/$'

    @rest_utils.ajax()
    def get(self, request, region):
        ret = {
            'visible': {
                'security_groups': False,
                'security_group_rules': False,
                'floatingips': False,
                'networks': False,
                'subnets': False,
                'routers': False,
                'buckets': False,
                'gigabytes': False,
            },
            'disabled': {
                'security_groups': False,
                'security_group_rules': False,
                'floatingips': False,
                'networks': False,
                'subnets': False,
                'routers': False,
                'buckets': False,
                'gigabytes': False,
            },
        }
        if region in settings.REGION_SETTINGS:
            ret = settings.REGION_SETTINGS[region]
        return http.JsonResponse(data=ret)
