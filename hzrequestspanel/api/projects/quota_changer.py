import prettytable
import urllib.parse
import yaml

import hzrequestspanel.api.projects

from hzrequestspanel.api.projects import LOG
from hzrequestspanel.api.projects import SnowException


class QuotaChanger(hzrequestspanel.api.projects.AbstractRequestCreator):
    def __init__(self, dict_data, **kwargs):
        super(QuotaChanger, self).__init__(dict_data, **kwargs)

        self.target_functional_element = self.config[
            'resources_functional_element']
        self.target_group = self.config['resources_group']

        self.title = (
            "[{1}] Request for quota change on Cloud Service Project "
            "name: {0}".format(
                self.dict_data['project_name'],
                self.dict_data['services_region'].upper()))
        self.user_message = """Dear %s,

Your quota update request has been received and sent to
HW Resources management in order to be evaluated.

Your request will be applied after approval.

Thank you,
        Cloud Infrastructure Team"""
        self.supporter_message = """Dear HW Resources manager,

Could you please review the following quota update request?

%s

You may find an overview of the Chargegroup's present resource utilization and quotas here:
https://monit-grafana.cern.ch/d/f11a13f7-90b7-402b-a43d-7a2c1d22ffd6/resource-coordination?%s

In order to apply these values, please execute the following Rundeck job:

https://cci-rundeck.cern.ch/project/HW-Resources/job/show/ad45c0a5-5a81-4861-a7ee-fbb7d54f122a?%s"

Best regards,
        Cloud Infrastructure Team"""   # noqa: E501

    def _verify_prerequisites(self):
        pass

    def _fill_ticket_with_proper_data(self):
        self.dict_data['username'] = self._get_primary_account_from_ldap(
            self.dict_data['username'])

        # Save temporarily the metadata values so they can be passed to
        # calculate the summary
        metadata = yaml.safe_dump(self.dict_data['metadata'])

        # Filter quota request and only send values that have changed
        if ('metadata' in self.dict_data.keys()
                and 'quota' in self.dict_data['metadata'].keys()
                and 'current' in self.dict_data['metadata'].keys()):
            self.cloudclient.filter_quota(
                self.dict_data['metadata']['quota'],
                self.dict_data['metadata']['current'],
            )
            # Send only quota changes in metadata not current values
            del self.dict_data['metadata']['current']

        self.dict_data['metadata'] = yaml.safe_dump(self.dict_data['metadata'])
        try:
            self.snowclient.record_producer.convert_RQF_to_quota_update(
                self.ticket, self.dict_data)
        except Exception as e:
            LOG.error("Error updating snow ticket: %s", e)
            raise SnowException(e)

        chargegroup = self._get_chargegroup_if_present()
        self._add_coordinators_to_watchlist(chargegroup)
        self.dict_data['chargegroup'] = chargegroup

        # Put back the metadata field so the summary can be calculated
        self.dict_data['metadata'] = metadata

    @staticmethod
    def __calculate_variation(current, requested):
        current = int(current)
        requested = int(requested)
        if current:
            return requested - current, (
                float(requested - current) / current) * 100
        else:
            if requested:
                return requested, 100
        return 0, 0

    def __summary_callback(self, region, service, key, resource, meta, table):
        if region not in meta['current'] or region not in meta['quota']:
            return
        if not resource:
            diff, percent = QuotaChanger.__calculate_variation(
                meta['current'][region][service][key],
                meta['quota'][region][service][key])
            variation = "%+d (%+d%%)" % (diff, percent)
            table.add_row([
                region,
                service,
                key,
                meta['current'][region][service][key],
                meta['quota'][region][service][key],
                variation
            ])
        else:
            if resource not in meta['current'][region][service] or \
                    resource not in meta['quota'][region][service]:
                return
            diff, percent = QuotaChanger.__calculate_variation(
                meta['current'][region][service][resource][key],
                meta['quota'][region][service][resource][key])
            variation = "%+d (%+d%%)" % (diff, percent)
            table.add_row([
                region,
                service,
                '%s - (%s)' % (key, resource),
                meta['current'][region][service][resource][key],
                meta['quota'][region][service][resource][key],
                variation
            ])

    def _request_summary(self):
        tableProject = prettytable.PrettyTable(
            ["Field", "Value"])
        tableProject.border = True
        tableProject.header = True
        tableProject.add_row(['Name', self.dict_data['project_name']])
        tableProject.add_row(['Chargegroup', self.dict_data['chargegroup']])

        comment = self.dict_data['comment']
        try:
            comment = comment.decode()
        except (UnicodeDecodeError, AttributeError):
            pass

        tableQuota = prettytable.PrettyTable(
            ["Region", "Service", "Quota", "Current",
             "Requested", "Variation"])

        self.cloudclient.service_quota_iterator(
            callback=self.__summary_callback,
            show_all=True,
            meta=yaml.safe_load(self.dict_data['metadata']),
            table=tableQuota)

        tableQuota.border = True
        tableQuota.header = True
        tableQuota.align["Region"] = 'c'
        tableQuota.align["Service"] = 'c'
        tableQuota.align["Quota"] = 'c'
        tableQuota.align["Current"] = 'c'
        tableQuota.align["Requested"] = 'c'
        tableQuota.align["Variation"] = 'c'

        return ("Project:\n{0}\n\n"
                "Comment:\n{1}\n\n"
                "Quota:\n{2}").format(tableProject, comment, tableQuota)

    def _generate_supporter_message(self):
        req_summary = self._request_summary()
        worknote_msg = self.supporter_message % (
            self._convert_to_monospace(req_summary),
            urllib.parse.urlencode(
                {
                    "orgId": 78,
                    "var-all_chargegroups": self._get_chargegroup_if_present(),
                    "var-region": ["cern", "pdc"],

                },
                doseq=True,
            ),
            urllib.parse.urlencode(
                {
                    "opt.snow_ticket": self.ticket.info.number,
                    "opt.behaviour": 'perform'

                },
            ),

        )
        return worknote_msg
