import prettytable
import urllib.parse
import yaml

import hzrequestspanel.api.projects
from hzrequestspanel.api.projects import LOG
from hzrequestspanel.api.projects import SnowException


class NewProjectCreator(hzrequestspanel.api.projects.AbstractRequestCreator):
    def __init__(self, dict_data, **kwargs):
        super(NewProjectCreator, self).__init__(dict_data, **kwargs)

        self.target_functional_element = (
            self.config['resources_functional_element']
        )
        self.target_group = self.config['resources_group']

        self.title = (
            "[{1}] Request for new shared Cloud Service Project "
            "name: {0}".format(
                self.dict_data['project_name'],
                self.dict_data['services_region'].upper()))
        self.user_message = """Dear %s,

Your project creation request has been received and sent to
HW Resources management in order to be evaluated.

Your request will be applied after approval.

Thank you,
        Cloud Infrastructure Team"""
        self.supporter_message = """Dear HW Resources manager,

Could you please review the following project creation request?

%s

If there are any special requests regarding non-standard flavours, please
reassign the ticket back to Cloud Team.

If not, in order to accept the request, please execute the following Rundeck job:

https://cci-rundeck.cern.ch/project/HW-Resources/job/show/14d9ba7f-5dbf-47b7-9142-5d9873fef80d?%s"

Best regards,
        Cloud Infrastructure Team"""   # noqa: E501

    def __summary_callback(self, region, service, key, resource, meta, table):
        if region not in meta['quota']:
            return
        if not resource:
            table.add_row([
                region,
                service,
                key,
                meta['quota'][region][service][key]
            ])
        else:
            table.add_row([
                region,
                service,
                '%s - (%s)' % (key, resource),
                meta['quota'][region][service][resource][key]
            ])

    def _request_summary(self):
        tableProject = prettytable.PrettyTable(
            ["Field", "Value"])
        tableProject.border = True
        tableProject.header = True
        tableProject.add_row(['Name', self.dict_data['project_name']])
        tableProject.add_row(['Description', self.dict_data['description']])
        tableProject.add_row(['Owner', self.dict_data['owner']])
        tableProject.add_row(['Egroup', self.dict_data['egroup']])
        tableProject.add_row(['Chargegroup', self.dict_data['chargegroup']])

        comment = self.dict_data['comment']
        try:
            comment = comment.decode()
        except (UnicodeDecodeError, AttributeError):
            pass

        tableQuota = prettytable.PrettyTable(
            ["Region", "Service", "Quota", "Requested"])

        self.cloudclient.service_quota_iterator(
            callback=self.__summary_callback,
            show_all=False,
            meta=yaml.safe_load(self.dict_data['metadata']),
            table=tableQuota)

        tableQuota.border = True
        tableQuota.header = True
        tableQuota.align["Region"] = 'c'
        tableQuota.align["Service"] = 'c'
        tableQuota.align["Quota"] = 'c'
        tableQuota.align["Requested"] = 'c'

        return ("Project:\n{0}\n\n"
                "Comment:\n{1}\n\n"
                "Quota:\n{2}").format(tableProject, comment, tableQuota)

    def _verify_prerequisites(self):
        self.dict_data['owner'] = self._get_primary_account_from_ldap(
            self.dict_data['owner'])

        for egroup in self.dict_data['egroup'].split(','):
            self._verify_egroup(egroup.strip())

    def _fill_ticket_with_proper_data(self):
        try:
            # self.dict_data['username'] = self.dict_data['owner']
            self.dict_data['username'] = self._get_primary_account_from_ldap(
                self.dict_data['username'])
            self.dict_data['metadata'] = yaml.safe_dump(
                self.dict_data['metadata'])
            self.snowclient.record_producer.convert_RQF_to_project_creation(
                self.ticket,
                self.dict_data)
        except Exception as e:
            LOG.error("Error updating snow ticket: %s", e)
            raise SnowException()

        self._add_coordinators_to_watchlist(self.dict_data['chargegroup'])

    def _generate_supporter_message(self):
        req_summary = self._request_summary()
        worknote_msg = self.supporter_message % (
            self._convert_to_monospace(req_summary),
            #  https://cci-rundeck.cern.ch/project/HW-Resources/job/show/14d9ba7f-5dbf-47b7-9142-5d9873fef80d?opt.snow_ticket=%s"
            urllib.parse.urlencode(
                {
                    "opt.snow_ticket": self.ticket.info.number,

                },
            ),
        )
        return worknote_msg
