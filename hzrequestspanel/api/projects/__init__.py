from abc import abstractmethod
import configparser
import logging
import subprocess  # nosec

from ccitools import common
from ccitools.utils.cloud import CloudRegionClient
from ccitools.utils.servicenowv2 import ServiceNowClient
from ccitools.utils.xldap import XldapClient

LOG = logging.getLogger('horizon.hzrequests')
LOG.setLevel(logging.INFO)


class SnowException(Exception):

    message = "Unable to create the ticket. Please retry. " \
              "If the problem persists, contact " \
              "the Cloud Team"

    def __init__(self, message=None):
        self.message = message or self.message
        super(SnowException, self).__init__(self.message)


class AbstractRequestCreator(object):
    def __init__(self,
                 dict_data,
                 config_file='/etc/openstack-dashboard/hzrequestspanel.conf',
                 keytab_file='/etc/openstack-dashboard/svcrdeck.keytab'):

        self.negociate_krb_ticket(keytab_file, 'svcrdeck@CERN.CH')

        self.dict_data = dict_data
        self.config = None
        self._parse_config_file(config_file)
        self.snowclient = self._create_snowclient_instance()
        self.cloudclient = self._create_cloudclient_instance()
        self.ticket = None
        self.user_message = None
        self.supporter_message = None
        self.username = dict_data['username']
        self.target_functional_element = None
        self.target_group = None

    def negociate_krb_ticket(self, path, username):
        kinit = '/usr/bin/kinit'
        kinit_args = [kinit, '-kt', path, username]
        kinit = subprocess.Popen(kinit_args)  # nosec
        kinit.wait()

    def _parse_config_file(self, config_file):
        try:
            self.config = configparser.ConfigParser()
            with open(config_file, 'r', encoding='utf-8') as conf_fp:
                self.config.read_file(conf_fp)

                config = {
                    'instance': self.config.get(
                        "servicenow", "instance"),
                    'cloud_functional_element': self.config.get(
                        "servicenow", "cloud_functional_element"),
                    'cloud_group': self.config.get(
                        "servicenow", "cloud_group"),
                    'resources_functional_element': self.config.get(
                        "servicenow", "resources_functional_element"),
                    'resources_group': self.config.get(
                        "servicenow", "resources_group"),
                    'watchlist_departments': [
                        dep.lower() for dep in self.config.get(
                            "servicenow", "watchlist_departments").split(',')],
                    'watchlist_egroup_template': self.config.get(
                        "servicenow", "watchlist_egroup_template"),
                    'group_blacklist': [
                        group.lower() for group in self.config.get(
                            "servicenow", "group_blacklist").split(',')],
                }

                self.config = config
        except Exception as e:
            # TODO(rbachman): Exception has no .message, use e directly
            LOG.error("Error parsing configuration: %s", e)
            raise SnowException()

    def _create_snowclient_instance(self):
        try:
            return ServiceNowClient(instance=self.config['instance'])
        except Exception as e:
            LOG.error("Error instanciating SNOW client: %s", e)
            raise SnowException()

    def _create_cloudclient_instance(self):
        try:
            return CloudRegionClient(cloud='cern')

        except Exception as e:
            LOG.error("Error instanciating cloud client: %s", e)
            raise SnowException(e)

    def _create_empty_snow_ticket(self, title):
        try:
            self.ticket = self.snowclient.ticket.create_RQF(
                title,
                self.target_functional_element,
                assignment_group=self.target_group)
        except Exception as e:
            LOG.error("Error creating empty SNOW ticket: %s", e)
            raise SnowException(e)

    def _create_notes_and_comments(self):
        try:
            user_info = self.snowclient.user.get_user_info_by_user_name(
                self.dict_data['username'])
            first_name = user_info.first_name

            self.ticket.add_comment(self.user_message % first_name)
            self.ticket.add_work_note(self._generate_supporter_message())

        except Exception:
            LOG.error("Error creating notes for "
                      "SNOW ticket %s", self.ticket.info.number)
            raise Exception(
                "Your ticket {0} has been successfully created, "
                "however we have identified some issues during the "
                "process. Please go to Service-Now and verify your "
                "request. If you find any problems, please contact "
                "the Cloud Team.".format(self.ticket.info.number))

    def create_ticket(self):
        LOG.info("Creating SNOW ticket with: %s", self.dict_data)
        self._verify_prerequisites()
        self._create_empty_snow_ticket(self.title)
        self._fill_ticket_with_proper_data()
        self.ticket.save()  # this save is to avoid losing the user's comment
        self._create_notes_and_comments()
        self.ticket.save()  # update ticket upstream
        LOG.info("SNOW ticket '%s' created successfully",
                 self.ticket.info.number)

        return self.ticket.info.number

    @staticmethod
    def _convert_to_monospace(text):
        text = "<br/>".join(text.split("\n"))
        text = "%s%s%s" % ("[code]<pre>", text, "</pre>[/code]")
        return text

    @staticmethod
    def _get_primary_account_from_ldap(username):
        try:
            xldap = XldapClient('ldap://xldap.cern.ch')
            return xldap.get_primary_account(username)
        except Exception:
            LOG.error(
                "Error creating SNOW ticket. '%s' "
                "is not a valid username", username)
            raise SnowException(
                "Unable to create the ticket. "
                "Username you have provided is not correct.")

    def _verify_egroup(self, name):
        try:
            if not self.cloudclient.is_group(name):
                raise Exception
        except Exception:
            LOG.error(
                "Error creating SNOW ticket. "
                "E-group not found: '%s'", name)
            raise SnowException(
                "Unable to create the ticket. "
                "E-group you have provided is not correct.")
        if name in self.config['group_blacklist']:
            LOG.error(
                "Error creating SNOW ticket. "
                "E-group can't be used: '%s'", name)
            raise SnowException(
                "Unable to create the ticket. "
                "E-group you have provided is not correct.")
        return True

    def _generate_supporter_message(self):
        req_summary = self._request_summary()
        worknote_msg = self.supporter_message % (
            self._convert_to_monospace(req_summary),
            self.ticket.info.number
        )
        return worknote_msg

    def _get_chargegroup_if_present(self):
        """Return the chargegroup corresponding to the request, if one exists.

        If none could be determined, returns an empty string.
        """
        try:
            project = self.cloudclient.find_project(
                self.dict_data['project_name'])
        except Exception as e:
            LOG.error(
                "Error retrieving information about the project: %s", e)
            return ''

        cg = ''
        if hasattr(project, 'chargegroup') and getattr(project, 'chargegroup'):
            chargegroups = common.get_chargegroups(
                uuid=project.chargegroup,
                inactive=True
            )
            if len(chargegroups) > 0:
                chargegroup = chargegroups[0]  # Assume single result only
                if chargegroup and 'name' in chargegroup.keys():
                    cg = chargegroup['name']
        return cg

    def _add_coordinators_to_watchlist(self, name):
        try:
            for dep in self.config['watchlist_departments']:
                if dep in name.lower().split():
                    self.ticket.add_email_to_watch_list(
                        self.config['watchlist_egroup_template'] % dep)
        except Exception as e:
            LOG.exception("Error adding coordinators to watchlist: %s", e)

    @abstractmethod
    def _verify_prerequisites(self):
        pass

    @abstractmethod
    def _fill_ticket_with_proper_data(self):
        pass

    @abstractmethod
    def _request_summary(self):
        pass
