HZ_REQUESTS_CONFIG = u"""
[servicenow]
instance=cerntraining
cloud_functional_element=Cloud Infrastructure
cloud_group=Cloud Infrastructure 3rd Line Support
resources_functional_element=HW Resources
resources_group=HW Resources 2nd Line Support
watchlist_departments=atlas,alice,cms,lhcb
watchlist_egroup_template=cloud-infrastructure-%%s-resource-coordinators@cern.ch
group_blacklist=cloud-infrastructure-users
[cloud]
user=svchorizon
pass=%%svc_pass%%
keystone_endpoint=https://keystone.cern.ch/main/v3
"""
