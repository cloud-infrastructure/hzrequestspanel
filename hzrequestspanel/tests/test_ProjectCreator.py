# coding: utf-8
import io
import logging
import unittest

from hzrequestspanel.api.projects.project_creator import NewProjectCreator
from hzrequestspanel.api.projects import SnowException
from hzrequestspanel.tests import fixtures
from unittest import mock

func = 'builtins.open'


class TestProjectCreator(unittest.TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.payload = {
            "ticket_type": "new_project",
            "username": "svchorizon",
            "comment": u"A spécial 人物",
            "chargegroup": "c1e42c89-f3fa-47db-ad31-e754128ef5e3",
            "project_name": "project-unittests",
            "services_region": "cern",
            "description": "unittest for hzrequestspanel",
            "owner": "svchorizon",
            "egroup": "cloud-infrastructure-3rd-level",
            "metadata": {
                "quota": {
                    'cern': {
                        'compute': {
                            'instances': 25,
                            'cores': 25,
                            'ram': 50
                        },
                        'blockstorage': {
                            'standard': {
                                'volumes': 0,
                                'gigabytes': 0,
                            },
                            'io1': {
                                'volumes': 0,
                                'gigabytes': 0,
                            },
                            'cp1': {
                                'volumes': 0,
                                'gigabytes': 0,
                            },
                            'cpio1': {
                                'volumes': 0,
                                'gigabytes': 0,
                            }
                        },
                        'fileshare': {
                            'Geneva CephFS Testing': {
                                'shares': 30,
                                'gigabytes': 100,
                            },
                            'Meyrin CephFS': {
                                'shares': 10,
                                'gigabytes': 200,
                            }
                        },
                        'object': {
                            'buckets': 20,
                            'gigabytes': 80,
                        },
                        'network': {
                            'loadbalancers': 20
                        }
                    }
                }
            }
        }

    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_create_ticket_positive(self,
                                    mock_io,
                                    mock_krb,
                                    mock_snow,
                                    mock_cloud,
                                    mock_xldap):
        mock_xldap.return_value.get_primary_account.return_value = 'fake'

        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.create_ticket()

    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_create_ticket_positive_requestor_primary_account(self,
                                                              mock_io,
                                                              mock_krb,
                                                              mock_snow,
                                                              mock_cloud,
                                                              mock_xldap):
        mock_xldap.return_value.get_primary_account.return_value = 'fake'

        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.dict_data['username'] = "makowals"
        request.create_ticket()

    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_create_ticket_positive_requestor_secondary_account(self,
                                                                mock_io,
                                                                mock_krb,
                                                                mock_snow,
                                                                mock_cloud,
                                                                mock_xldap):
        mock_xldap.return_value.get_primary_account.return_value = 'jcastro'
        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.dict_data['username'] = "admjcast"
        request.create_ticket()

    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_create_ticket_positive_multiple_egroups(self,
                                                     mock_io,
                                                     mock_krb,
                                                     mock_snow,
                                                     mock_cloud,
                                                     mock_xldap):
        mock_xldap.return_value.get_primary_account.return_value = 'fake'
        egroups = ("cloud-infrastructure-3rd-level,"
                   "cloud-infrastructure-baremetal-admin")

        mock_snow.return_value.get_project_creation_rp.return_value.egroup = (
            egroups)

        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.dict_data['egroup'] = egroups
        request.create_ticket()
        record_producer = request.snowclient.get_project_creation_rp(
            request.ticket.info.number)
        self.assertEqual(record_producer.egroup, egroups)

    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_create_ticket_wrong_egroup(self,
                                        mock_io,
                                        mock_krb,
                                        mock_snow,
                                        mock_cloud,
                                        mock_xldap):
        mock_xldap.return_value.get_primary_account.return_value = 'fake'
        mock_cloud.return_value.is_group.side_effect = iter([Exception])

        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.dict_data['egroup'] = "svchorizon"
        with self.assertRaises(SnowException):
            request.create_ticket()

    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_create_ticket_egroup_in_blacklist(self,
                                               mock_io,
                                               mock_krb,
                                               mock_snow,
                                               mock_cloud,
                                               mock_xldap):
        mock_xldap.return_value.get_primary_account.return_value = 'fake'

        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.dict_data['egroup'] = "cloud-infrastructure-users"
        with self.assertRaises(SnowException):
            request.create_ticket()

    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_get_primary_account_from_ldap(self,
                                           mock_io,
                                           mock_krb,
                                           mock_snow,
                                           mock_cloud,
                                           mock_xldap):
        mock_xldap.return_value.get_primary_account.return_value = 'fake'

        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        self.assertEqual(
            request._get_primary_account_from_ldap(
                "svchorizon"), "fake")
        self.assertNotEqual(
            request._get_primary_account_from_ldap(
                "svchorizon"), "svchorizon")

    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_verify_egroup_positive(self,
                                    mock_io,
                                    mock_krb,
                                    mock_snow,
                                    mock_cloud,
                                    mock_xldap):
        mock_xldap.return_value.get_primary_account.return_value = 'fake'
        mock_cloud.return_value.is_group.return_value = True

        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        self.assertTrue(
            request._verify_egroup("cloud-infrastructure-3rd-level"))

    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_verify_egroup_negative(self,
                                    mock_io,
                                    mock_krb,
                                    mock_snow,
                                    mock_cloud,
                                    mock_xldap):
        mock_xldap.return_value.get_primary_account.return_value = 'fake'
        mock_cloud.return_value.is_group.return_value = False

        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        with self.assertRaises(SnowException):
            request._verify_egroup("fake-group")

    def _test_add_coordinators_to_watchlist(self, experiment):
        request = NewProjectCreator(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.dict_data['chargegroup'] = experiment
        request.create_ticket()
        watch_list = request.ticket.info.watch_list
        self.assertEqual(
            watch_list,
            "cloud-infrastructure-%s-resource-coordinators@cern.ch" % (
                experiment))
