# coding: utf-8
import io
import logging
import unittest

from hzrequestspanel.api.projects.project_killer import ProjectKiller
from hzrequestspanel.api.projects import SnowException
from hzrequestspanel.tests import fixtures
from unittest import mock

func = 'builtins.open'


class TestProjectKiller(unittest.TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.payload = {
            "ticket_type": "delete_project",
            "username": "jcastro",
            "project_name": "Personal jcastro",
            "comment": u"A spécial 人物"
        }

    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_verify_user_role_positive(self,
                                       mock_io,
                                       mock_krb,
                                       mock_snow,
                                       mock_cloud):
        mock_cloud.return_value.check_user_has_role.return_value = True
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        request = ProjectKiller(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request._verify_user_role("Personal jcastro", "jcastro")

    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_verify_user_role_negative(self,
                                       mock_io,
                                       mock_krb,
                                       mock_snow,
                                       mock_cloud):
        mock_cloud.return_value.check_user_has_role.return_value = False
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        request = ProjectKiller(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        with self.assertRaises(SnowException):
            request._verify_user_role("Personal jcastro", "makowals")

    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_create_ticket_positive(self,
                                    mock_io,
                                    mock_krb,
                                    mock_snow,
                                    mock_cloud):
        mock_cloud.return_value.check_user_has_role.return_value = True
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        request = ProjectKiller(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.create_ticket()

    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_create_ticket_wrong_owner(self,
                                       mock_io,
                                       mock_krb,
                                       mock_snow,
                                       mock_cloud):
        mock_cloud.return_value.check_user_has_role.return_value = False
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        request = ProjectKiller(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.dict_data['username'] = "wiebalck"
        with self.assertRaises(SnowException):
            request.create_ticket()

    def _test_add_coordinators_to_watchlist(self, experiment):

        def _project_with_chargegroup(project_name):
            project = mock.MagicMock()
            setattr(project, 'chargegroup', experiment)
            return project

        request = ProjectKiller(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.cloudclient.find_project = mock.MagicMock(
            side_effect=_project_with_chargegroup)
        request.create_ticket()
        watch_list = request.ticket.info.watch_list
        self.assertTrue(
            "cloud-infrastructure-%s-resource-coordinators@cern.ch" % (
                experiment) in watch_list)

    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_add_coordinators_to_watchlist_atlas(self,
                                                 mock_io,
                                                 mock_krb,
                                                 mock_snow,
                                                 mock_cloud):
        mock_cloud.return_value.get_project_owner.return_value = 'jcastro'
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        mock_snow.return_value.ticket.create_RQF.\
            return_value.info.watch_list = (
                "cloud-infrastructure-atlas-resource-coordinators@cern.ch")
        self._test_add_coordinators_to_watchlist("atlas")

    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_add_coordinators_to_watchlist_cms(self,
                                               mock_io,
                                               mock_krb,
                                               mock_snow,
                                               mock_cloud):
        mock_cloud.return_value.get_project_owner.return_value = 'jcastro'
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        mock_snow.return_value.ticket.create_RQF.\
            return_value.info.watch_list = (
                "cloud-infrastructure-cms-resource-coordinators@cern.ch")
        self._test_add_coordinators_to_watchlist("cms")

    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.project_creator.'
                'NewProjectCreator.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_add_coordinators_to_watchlist_lhcb(self,
                                                mock_io,
                                                mock_krb,
                                                mock_snow,
                                                mock_cloud):
        mock_cloud.return_value.get_project_owner.return_value = 'jcastro'
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        mock_snow.return_value.ticket.create_RQF.\
            return_value.info.watch_list = (
                "cloud-infrastructure-lhcb-resource-coordinators@cern.ch")
        self._test_add_coordinators_to_watchlist("lhcb")
