# coding: utf-8
import io
import logging
import unittest

from hzrequestspanel.api.projects.quota_changer import QuotaChanger
from hzrequestspanel.tests import fixtures
from unittest import mock

func = 'builtins.open'


class TestQuotaChanger(unittest.TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.payload = {
            "ticket_type": "quota_change",
            "username": "svchorizon",
            "project_name": "Personal jcastro",
            "services_region": "cern",
            "comment": u"A spécial 人物",
            "metadata": {
                "quota": {
                    'cern': {
                        'compute': {
                            'instances': 25,
                            'cores': 25,
                            'ram': 50
                        },
                        'blockstorage': {
                            'standard': {
                                'volumes': 5,
                                'gigabytes': 500,
                            },
                            'io1': {
                                'volumes': 0,
                                'gigabytes': 0,
                            },
                            'cp1': {
                                'volumes': 0,
                                'gigabytes': 0,
                            },
                            'cpio1': {
                                'volumes': 0,
                                'gigabytes': 0,
                            }
                        },
                        'fileshare': {
                            'Geneva CephFS Testing': {
                                'shares': 30,
                                'gigabytes': 100,
                            },
                            'Meyrin CephFS': {
                                'shares': 10,
                                'gigabytes': 200,
                            }
                        },
                        'object': {
                            'buckets': 20,
                            'gigabytes': 80,
                        },
                        'network': {
                            'loadbalancers': 20,
                        }
                    }
                },
                "current": {
                    'cern': {
                        'compute': {
                            'instances': 25,
                            'cores': 25,
                            'ram': 50
                        },
                        'blockstorage': {
                            'standard': {
                                'volumes': 5,
                                'gigabytes': 500,
                            },
                            'io1': {
                                'volumes': 0,
                                'gigabytes': 0,
                            },
                            'cp1': {
                                'volumes': 0,
                                'gigabytes': 0,
                            },
                            'cpio1': {
                                'volumes': 0,
                                'gigabytes': 0,
                            }
                        },
                        'fileshare': {
                            'Geneva CephFS Testing': {
                                'shares': 10,
                                'gigabytes': 100,
                            },
                            'Meyrin CephFS': {
                                'shares': 50,
                                'gigabytes': 150,
                            }
                        },
                        'object': {
                            'buckets': 10,
                            'gigabytes': 10,
                        },
                        'network': {
                            'loadbalancers': 10,
                        }
                    }
                }
            }
        }

    @mock.patch('ccitools.common.get_chargegroups')
    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.quota_changer.'
                'QuotaChanger.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_create_ticket_positive(self,
                                    mock_io,
                                    mock_krb,
                                    mock_snow,
                                    mock_cloud,
                                    mock_xldap,
                                    mock_chgs):
        mock_xldap.return_value.get_primary_account.return_value = 'jcastro'
        mock_chgs.return_value = [{'name': 'IT'}]

        request = QuotaChanger(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.create_ticket()

    def _test_add_coordinators_to_watchlist(self, experiment):
        def _project_with_chargegroup(project_name):
            project = mock.MagicMock()
            setattr(project, 'chargegroup', experiment)
            return project

        request = QuotaChanger(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.cloudclient.find_project = (
            mock.MagicMock(side_effect=_project_with_chargegroup)
        )
        request.dict_data['project_name'] = (
            request.dict_data['project_name']
        )
        request.create_ticket()
        watch_list = request.ticket.info.watch_list
        self.assertEqual(
            watch_list,
            "cloud-infrastructure-%s-resource-coordinators@cern.ch" % (
                experiment)
        )

    @mock.patch('ccitools.common.get_chargegroups')
    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.quota_changer.'
                'QuotaChanger.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_add_coordinators_to_watchlist_atlas(self,
                                                 mock_io,
                                                 mock_krb,
                                                 mock_snow,
                                                 mock_cloud,
                                                 mock_xldap,
                                                 mock_chgs):
        mock_xldap.return_value.get_primary_account.return_value = 'jcastro'
        mock_cloud.return_value.get_project_owner.return_value = 'jcastro'
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        mock_chgs.return_value = [{'name': 'atlas'}]
        mock_snow.return_value.ticket.create_RQF.\
            return_value.info.watch_list = (
                "cloud-infrastructure-atlas-resource-coordinators@cern.ch")
        self._test_add_coordinators_to_watchlist("atlas")

    @mock.patch('ccitools.common.get_chargegroups')
    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.quota_changer.'
                'QuotaChanger.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_add_coordinators_to_watchlist_cms(self,
                                               mock_io,
                                               mock_krb,
                                               mock_snow,
                                               mock_cloud,
                                               mock_xldap,
                                               mock_chgs):
        mock_xldap.return_value.get_primary_account.return_value = 'jcastro'
        mock_cloud.return_value.get_project_owner.return_value = 'jcastro'
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        mock_chgs.return_value = [{'name': 'cms'}]
        mock_snow.return_value.ticket.create_RQF.\
            return_value.info.watch_list = (
                "cloud-infrastructure-cms-resource-coordinators@cern.ch")
        self._test_add_coordinators_to_watchlist("cms")

    @mock.patch('ccitools.common.get_chargegroups')
    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.quota_changer.'
                'QuotaChanger.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_add_coordinators_to_watchlist_lhcb(self,
                                                mock_io,
                                                mock_krb,
                                                mock_snow,
                                                mock_cloud,
                                                mock_xldap,
                                                mock_chgs):
        mock_xldap.return_value.get_primary_account.return_value = 'jcastro'
        mock_cloud.return_value.get_project_owner.return_value = 'jcastro'
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        mock_chgs.return_value = [{'name': 'lhcb'}]
        mock_snow.return_value.ticket.create_RQF.\
            return_value.info.watch_list = (
                "cloud-infrastructure-lhcb-resource-coordinators@cern.ch")
        self._test_add_coordinators_to_watchlist("lhcb")

    @mock.patch('ccitools.common.get_chargegroups')
    @mock.patch('hzrequestspanel.api.projects.XldapClient')
    @mock.patch('hzrequestspanel.api.projects.CloudRegionClient')
    @mock.patch('hzrequestspanel.api.projects.ServiceNowClient')
    @mock.patch('hzrequestspanel.api.projects.quota_changer.'
                'QuotaChanger.negociate_krb_ticket')
    @mock.patch(func, return_value=io.StringIO(fixtures.HZ_REQUESTS_CONFIG))
    def test_get_primary_account_from_ldap(self,
                                           mock_io,
                                           mock_krb,
                                           mock_snow,
                                           mock_cloud,
                                           mock_xldap,
                                           mock_chgs):
        mock_xldap.return_value.get_primary_account.return_value = 'jcastro'
        mock_cloud.return_value.get_project_owner.return_value = 'jcastro'
        mock_cloud.return_value.get_project_members.return_value = 'jcastro'
        mock_chgs.return_value = [{'name': 'IT'}]

        request = QuotaChanger(self.payload)
        request._create_empty_snow_ticket("Unit tests for hzrequestspanel")
        request.create_ticket()

        self.assertEqual(
            request._get_primary_account_from_ldap(
                "svchorizon"), "jcastro")
        self.assertNotEqual(
            request._get_primary_account_from_ldap(
                "svchorizon"), "svchorizon")
