from django.utils.translation import gettext_lazy as _

import horizon

from hzrequestspanel import dashboard


class SupportPanel(horizon.Panel):
    name = _("Support")
    slug = "support"


dashboard.HZRequests.register(SupportPanel)
