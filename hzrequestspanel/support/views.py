from django.utils.translation import gettext_lazy as _
from horizon import views


class IndexView(views.APIView):
    template_name = 'hzrequestspanel/support/index.html'
    page_title = _("Support")
