DASHBOARD = 'hzrequests'

ADD_INSTALLED_APPS = ['hzrequestspanel']

DEFAULT = True

ADD_ANGULAR_MODULES = [
    'hzrequests.workflow',
    'hzrequests.validators'
]

AUTO_DISCOVER_STATIC_FILES = True

ADD_SCSS_FILES = [
    'hzrequests/hzrequestspanel.scss',
]
