Name:       python-hzrequestspanel
Version:    3.5.3
Release:    2%{?dist}
Summary:    OpenStack Dashboard panel plugin for Service Now requests
License:    ASL 2.0
URL:        https://gitlab.cern.ch/cloud-infrastructure/hzrequestspanel
Source0:    %{name}-%{version}.tar.gz
BuildArch:  noarch
Group:      CERN/Utilities

BuildRequires:  python3-devel
BuildRequires:  python3-pbr
BuildRequires:  python3-setuptools

%{?systemd_requires}
BuildRequires:  systemd

%description
OpenStack Dashboard plugin to create a new panel, under Project dashboard,
to manage Requests to Service Now

%package -n python3-hzrequestspanel
Summary:    OpenStack Dashboard panel plugin for Service Now requests

Requires:   cci-tools
Requires:   openstack-dashboard
Requires:   python3-django
Requires:   python3-django-horizon
Requires:   python3-keystoneclient
Requires:   python3-cinderclient
Requires:   python3-novaclient
Requires:   python3-radosgw-admin

%description -n python3-hzrequestspanel
OpenStack Dashboard plugin to create a new panel, under Project dashboard,
to manage Requests to Service Now

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%py3_build

%install
%py3_install

# Install enabled file for the plugin
install -p -D -m 644 enabled/_9999_hzrequests.py %{buildroot}%{_datadir}/openstack-dashboard/openstack_dashboard/enabled/_9999_hzrequests.py

%post
%systemd_post httpd.service

%preun
%systemd_preun httpd.service

%postun
%systemd_postun_with_restart httpd.service

%files -n python3-hzrequestspanel
%doc README.md
%{python3_sitelib}/hzrequestspanel
%{python3_sitelib}/hzrequestspanel-*-py*.egg-info
%{_datadir}/openstack-dashboard/openstack_dashboard/enabled/_9999_hzrequests.py*

%changelog
* Tue Mar 04 2025 Jose Castro Leon <jose.castro.leon@cern.ch> - 3.5.3-2
- Static adjustments, href url fix

* Wed Jan 22 2025 Richard Bachmann <richard.bachmann@cern.ch> - 3.5.3-1
- Make project creation comment field mandatory

* Thu Nov 14 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 3.5.2-3
- Update lb regions to include poc+pdc

* Thu Nov 07 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 3.5.2-2
- Fix for multi-word project names

* Thu Nov 07 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 3.5.2-1
- Add Resource Coordinator dashboard url to quota form, and make comment mandatory
- Fix LB default ratio for members and health_monitors

* Wed Aug 07 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 3.5.1-2
- Only enable loadbalancer form on cern region for the moment

* Mon Jul 08 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 3.5.1-1
- Adapt defaults for ports, security_groups and rules
- Remove dirty from all flows

* Tue Jun 25 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.5.0-2
- Allow to hide object quotas if needed

* Fri Jun 14 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.5.0-1
- Add network and loadbalancer extended quotas

* Mon Mar 18 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 3.4.0-1
- Fix error labels not shown due to wrong form name in validators
- Fix error labels not shown in red when in error, applied same form-group logic for has-error.
- Add minimun value in error string to facilitate fixing the form   when user add low values beyond the current usage.
- Added backup section in volumes for creation and update

* Wed Nov 15 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 3.3.0-1
- Handle multi region creation and quota requests

* Fri Nov 03 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 3.2.0-1
- Remove floating_ips from network quota
- Use region in the session to find volume types and share types
- Remove network from the list of services on demand as it's now by default
- Add new volumes in the volume priority list
- Remove references of sdn1 in the code

* Tue Sep 05 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.9-4
- Rebuild for yoga release

* Mon Jul 03 2023 Domingo Rivera Barros <driverab@cern.ch> 3.1.9-3
- OS-16891: Rundeck job URL in one line to avoid <div> insertion in emails

* Wed Nov 23 2022 Domingo Rivera Barros <driverab@cern.ch> 3.1.9-2
- Add python-landbclient to requirements.txt
- OS-16369: Add info about shares in the critical area

* Mon May 09 2022 Domingo Rivera Barros <driverab@cern.ch> 3.1.9-1
- OS-15785: Modify monitoring URL

* Tue Apr 26 2022 Domingo Rivera Barros <driverab@cern.ch> 3.1.8-2
- Fix loading of inactive chargegroups

* Mon Apr 25 2022 Domingo Rivera Barros <driverab@cern.ch> 3.1.8-1
- OS-15946: Load inactive chargegroups

* Fri Dec 03 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.7-2
- Update link to documentation

* Thu Nov 18 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.7-1
- Add group_blacklist option to ignore certain egroups

* Wed Sep 22 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.6-3
- Hide chart definitions if there is nothing to show

* Fri Sep 17 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.6-2
- Look for exact match in chargegroup name

* Fri Sep 17 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.6-1
- Fix issue while adding coordinators in the watchlist

* Mon May 03 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.5-3
- Improve caching of chargegroups

* Thu Apr 29 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.5-2
- Fix round calculation of free resources

* Wed Apr 28 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.5-1
- Set landb responsible and mainuser to true as group defaults
- Add overrides for cloud website

* Tue Apr 27 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.4-1
- Add keep ratio for compute instances/cores/ram
- Improve selector for chargegroup
- Remove python2 compatibility

* Tue Apr 20 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.3-2
- Improve UI behavior for projects with unlimited quota

* Tue Apr 20 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.3-1
- Replace piecharts by bars for showing project quota

* Wed Apr 14 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.2-1
- Handle chargegroups for update quota requests
- Calculate chargegroup internally in quota update
- Improve readability in snow for requests
- Lower compute quota defaults
- Add extra information about the project in the snow requests
- Move chargegroups logic to ccitools
- Add CI changes to build on centos 8stream
- Refactor chargegroup retrieval and use it for watchlist on ticket creation

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.1-1
- Simplify tests
- Use is_group from cloudclient class instead of xldap
- Add missing sanity check for compute
- Enforce input data sanitization on project creation and quota requests
- Use centralized quotas

* Thu Mar 18 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.1.0-1
- Add management on project chargegroups

* Mon Mar 15 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.1-7
- Disable temporarily floating ips

* Mon Feb 15 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.1-6
- Add department SY in the list of departments

* Wed Jan 27 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.1-5
- Add missing js to process new type metadata information

* Wed Jan 27 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.1-4
- Handle extra metadata information from volume types and share types

* Wed Jan 20 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.1-3
- Do not show regions in which the user has no quota

* Fri Dec 04 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.1-2
- Add missing ng-required in the description field for project creation

* Fri Dec 04 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.1-1
- Add support to define default landb mainuser and/or responsible in the project request

* Wed Nov 25 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.0-3
- Add requester as default owner in project creation wizard
- Modify text on egroups in project creation wizard

* Tue Nov 24 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.0-2
- Make s3 quota calculation an integer result

* Tue Nov 24 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.0.0-1
- Replace dialogs for wizards on project creation, deletion and quota update
- Added async validation of owner and groups
- Added information about volume type metadata and share type metadata

* Mon Nov 09 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.5.0-7
- Add coordinators only if accounting-group property is present
- monitoring link - organization ID specified

* Tue Nov 03 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.5.0-6
- Add links and buttons in Overview “hzrequestspanel”
- monitoring link added
- OS-12248: Prevent creation of requests with empty quota

* Tue Oct 27 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.5.0-5
-  Fix deletion of projects

* Tue Oct 27 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.5.0-4
-  Another adjustment of css in this case project deletion

* Tue Oct 27 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.5.0-3
- Change behavior of buttons in index view
- Make dialog a bit smaller to better fit into screens

* Fri Oct 23 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.5.0-2
- Change requests panel name by overview
- Set this panel as the default landing page in horizon

* Fri Oct 16 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.5.0-1
- Convert into dashboard to simplify integration

* Tue Oct 13 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4.2-5
- Do not show only public share types in the list

* Tue Oct 13 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4.2-4
- Ignore types in which the project has no quota

* Tue Oct 13 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4.2-3
- Use show_all parameter to iterate over all types

* Mon Oct 12 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4.2-2
- Adapt to new record producers for snow

* Wed Sep 23 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4.2-1
- Use an api call to fetch experiment list
- Use snow metadata

* Wed Jul 01 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4.1-2
- Fix dependency issue on centos7

* Tue Jun 23 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4.1-1
- Rebuild to make it compatible to centos7 and centos8

* Tue Jun 23 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-31
- Adapt to new cloudregionclient authentication method

* Fri Nov 22 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-30
- Do not configure additional clients on cloudregionclient

* Thu Sep 26 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-29
- Fix bug on retrieving quota for sdn1 region

* Thu Sep 26 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-28
- Initial version with network quotas retrieval

* Wed Sep 25 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-27
- Improve network handling of create project and update quota

* Wed Sep 25 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-26
- Implement needed bits for quota_change scenario
- Add loadbalancers and floating ips into new project request

* Tue Sep 17 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-25
- Add FASER as an experiment in the selection box
- Remove wigner volume types

* Thu Aug 29 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-24
- Replace cirundeck alias by cci-rundeck
- Escape special characters from project name pattern

* Fri Jul 26 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-23
- Inclusion of test framework to maintain python2 and python3 compatibility
- Fix pep8 issues to improve code
- Adapt to changes in cci-tools

* Mon Mar 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-22
- Fix get_project_owner call

* Mon Mar 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-21
- Use volume type list in ccitools

* Tue Nov 27 2018 Theodoros Tsioutsias <theodoros.tsioutsias@cern.ch> 2.4-20
- [OS-7693] Add s3 usage in project overview
- [OS-7693] Enable Object Store request panel

* Thu Oct 18 2018 Luis Pigueiras <luis.pigueiras@cern.ch> 2.4-19
- [OS-7658] Fix add experiment coordinators to quota change tickets
- [OS-7658] Add experiment coordinators to deletion project tickets

* Thu Jul 19 2018 Theodoros Tsioutsias <theodoros.tsioutsias@cern.ch> 2.4-18
- Add shares and s3 request panels

* Mon Jun 04 2018 Theodoros Tsioutsias <theodoros.tsioutsias@cern.ch> 2.4-17
- Fix hzrequestspanel view to inherit from ProjectUsageView

* Fri Jun 01 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.4-16
- Fix issue on overview with angularjs in Queens release

* Thu Apr 19 2018 Theodoros Tsioutsias <theodoros.tsioutsias@cern.ch> 2.4-15
- Explicit project name in deletion reqs

* Wed Dec 20 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 2.4-13
- Add GBAR to the list of experiments

* Mon Nov 13 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 2.4-9
- Support for volume type quotas in new project request
- Support for multiple e-groups in new project request

* Mon Sep 04 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 2.4-1
- Support for SnowClientV2

* Fri Jun 30 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 2.2-1
- Create SNOW ticket directly against target Functional Element

* Wed Jun 14 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 2.1-1
- Add unit tests, refactor code and release Delete and Create Project forms

* Thu Jun 08 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 2.0-10
- Added Delete Project Request form

* Wed May 10 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 2.0-1
- Complete refactoring of this code
- Added Create New Project Request form

* Tue May 09 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 1.6-1
- Fix link to Quota History Dashboard (force selecting organization)

* Tue Mar 07 2017 Mateusz Kowalski <mateusz.kowalski@cern.ch> 1.5-1
- Automatically restart httpd when upgrading package

* Wed Dec 07 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> 1.2-1
- Prefill job URL with snow_ticket
- Query Active Directory to check if user existance

* Fri Nov 04 2016 Mateusz Kowalski <mateusz.kowalski@cern.ch> 1.0-3
- Merge changes from Horizon newton

* Thu Sep 22 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> 1.0-1
- Change quota update request worknote message
- Add Makefile.koji to project

* Thu Jun 23 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 0.2-3
- OS-3092 Improve error message for request quota change action

* Wed Jun 15 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 0.2-2
- Fix RAM data to send (OS-2873) and set proper % when the initial value is 0

* Wed May 04 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 0.2-1
- OS-2873 Escalate request quota change SNOW ticket

* Wed Mar 23 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 0.1-5
- Change ng-mouse and ng-change to ng-blur in volume type inputs

* Thu Mar 10 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 0.1-4
- Fix + char in compute % values
- Align left volume list headers

* Thu Jan 14 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 0.1-3
- First RPM
