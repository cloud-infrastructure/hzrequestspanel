hzrequestspanel
===============


[![pipeline status](https://gitlab.cern.ch/cloud-infrastructure/hzrequestspanel/badges/master/pipeline.svg)](https://gitlab.cern.ch/cloud-infrastructure/hzrequestspanel/-/commits/master)
[![coverage report](https://gitlab.cern.ch/cloud-infrastructure/hzrequestspanel/badges/master/coverage.svg)](https://gitlab.cern.ch/cloud-infrastructure/hzrequestspanel/-/commits/master)

Horizon plugin that contains customizations for a better integratation in the CERN environment

Following the plugin documentation https://review.openstack.org/#/c/233709/12/doc/source/tutorials/plugin.rst
